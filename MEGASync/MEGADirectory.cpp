//
// Created by nikita on 2/5/19.
//
#include "MEGADirectory.h"

MEGADirectory::MEGADirectory(Client *client) : client(client) {}

MEGADirectory::~MEGADirectory() {
  if (remove) {
    removeDir();
  }
}

void MEGADirectory::setDirs(const string &localPath, const string &remotePath) {
  if (!std::filesystem::exists(localPath)) {
    std::filesystem::create_directories(localPath);
    std::filesystem::create_directory(localPath);
  }
  localDir = std::filesystem::canonical(localPath);
  remoteDir = client->getClient()->childnodebyname(client->getClient()->nodebyhandle(client->getClient()->rootnodes[0]),
                                                   remotePath.c_str());
  if (remoteDir == nullptr) {
    string newName = remotePath;
    SymmCipher key;
    string attrstring;
    Byte buf[FOLDERNODEKEYLENGTH];
    auto newNode = new NewNode[1];
    newNode[0].source = NEW_NODE;
    newNode[0].type = FOLDERNODE;
    newNode[0].nodehandle = 0;
    newNode[0].parenthandle = UNDEF;
    client->getClient()->rng.genblock(buf, FOLDERNODEKEYLENGTH);
    newNode[0].nodekey.assign((char *) buf, FOLDERNODEKEYLENGTH);
    key.setkey(buf);
    AttrMap attrs;
    client->getClient()->fsaccess->normalize(&newName);
    attrs.map['n'] = newName;
    attrs.getjson(&attrstring);
    newNode[0].attrstring = new string;
    client->getClient()->makeattr(&key, newNode[0].attrstring, attrstring.c_str());
    client->getApp()->putNodes(client->getClient()->rootnodes[0], newNode, 1);
    client->getApp()->waitUntilNodesUpdate();
    remoteDir = client->getClient()->childnodebyname(
        client->getClient()->nodebyhandle(client->getClient()->rootnodes[0]), remotePath.c_str());
  }
}

double MEGADirectory::getLoginProgress() const {
  return client->getApp()->getLoginProgress();
}

double MEGADirectory::getLoadProgress() const {
  return client->getApp()->getLoadProgress();
}

void MEGADirectory::getFile(const string &name) {
  Node *node = client->getClient()->childnodebyname(remoteDir, name.c_str());
  if (node == nullptr) {
    throw logic_error("remote file doesn't exists");
  }
  File *f = new File;
  f->FileFingerprint::operator=(*node);
  f->localname = localDir / name;
  f->h = node->nodehandle;
  f->name = node->displayname();
  client->getApp()->getFile(f);
  client->getApp()->waitUntilLoad();
  if (!client->getApp()->isOk()) {
    throw runtime_error(client->getApp()->getError());
  }
}

void MEGADirectory::putFile(const string &name) {
  ifstream s(localDir / name);
  if (s.fail()) {
    throw logic_error("local file doesn't exists");
  }
  File *f = new File;
  f->localname = localDir / name;
  f->h = remoteDir->nodehandle;
  f->name = name;
  client->getApp()->putFile(f);
  client->getApp()->waitUntilLoad();
  /*while (client->getApp()->isOk() && !client->getApp()->isTransferFinished()) {
    client->getApp()->waitUntilLoadFor(1s);
    cout << "put file to " << client->getApp()->getLoadProgress() << endl;
  }*/

  client->getApp()->waitUntilNodesUpdate();
  if (!client->getApp()->isOk()) {
    throw runtime_error(client->getApp()->getError());
  }
}

MEGADirectory::iterator MEGADirectory::begin() const {
  return MEGADirectory::iterator(remoteDir);
}

MEGADirectory::iterator MEGADirectory::end() const {
  return MEGADirectory::iterator();
}

void MEGADirectory::removeAfterDestruction(bool n_remove) {
  remove = n_remove;
}

void MEGADirectory::removeDir() {
  client->getApp()->removeNode(remoteDir);
  client->getApp()->waitUntilUnlink();
  client->getApp()->waitUntilNodesUpdate();
}

void MEGADirectory::removeFile(const string &name) {
  Node *node = client->getClient()->childnodebyname(remoteDir, name.c_str());
  if (node == nullptr) {
    throw logic_error("remote file doesn't exists");
  }
  client->getApp()->removeNode(node);
  client->getApp()->waitUntilUnlink();
  client->getApp()->waitUntilNodesUpdate();
}

MEGADirectory::iterator::iterator() : remoteDir(nullptr) {}

MEGADirectory::iterator::iterator(Node *remoteDir) : remoteDir(remoteDir) {
  it = remoteDir->children.cbegin();
}

MEGADirectory::iterator &MEGADirectory::iterator::operator++() {
  it++;
  return *this;
}

bool MEGADirectory::iterator::operator==(const MEGADirectory::iterator &right) const {
  return remoteDir == right.remoteDir && (remoteDir == nullptr ? true : it == right.it);
}

const string MEGADirectory::iterator::operator*() const {
  return (*it)->displayname();
}

bool MEGADirectory::iterator::operator!=(const MEGADirectory::iterator &right) const {
  return !operator==(right);
}

MEGADirectory::iterator begin(const MEGADirectory &dir) {
  return dir.begin();
}

MEGADirectory::iterator end(const MEGADirectory &dir) {
  return dir.end();
}

