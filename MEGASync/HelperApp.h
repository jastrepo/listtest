//
// Created by nikita on 2/16/19.
//

#ifndef LISTTEST_HELPERAPP_H
#define LISTTEST_HELPERAPP_H

#include <mega.h>
#include <condition_variable>

using namespace std;
using namespace mega;

typedef unsigned char Byte;

class HelperApp : public MegaApp {
  atomic<bool> ok, loggedIn, internalError, xferFinish, nodesUpdate, unlinked, waitForLogin, waitForLoad, waitForNewNodes, waitForUnlink;
  atomic<error> errorType;
  condition_variable cv;
  mutex m;
  string email, password;
  atomic<double> loginProgress, loadProgress;
public:
  void setError(error newErrorType);

  void setInternal();

  HelperApp();

  void logIn(const string &n_email, const string &n_password);

  void putFile(File *file);

  void getFile(File *file);

  void removeNode(Node *node);

  void putNodes(handle targetDir, NewNode *nodes, int number);

  bool isOk();

  bool isLoggedIn();

  bool isTransferFinished();

  string getError();

  void waitUntilLogin();

  template<class Rep, class Period>
  void waitUntilLoginFor(const std::chrono::duration<Rep, Period> &rel_time) {
    if (ok && !loggedIn && !waitForLogin) {
      waitForLogin = true;
      unique_lock<mutex> ul(m);
      cv.wait_for(ul, rel_time, [&] { return !ok || loggedIn; });
      waitForLogin = false;
    }
  }

  void waitUntilLoad();

  template<class Rep, class Period>
  void waitUntilLoadFor(const std::chrono::duration<Rep, Period> &rel_time) {
    if (ok && loggedIn && !xferFinish && !waitForLoad) {
      waitForLoad = true;
      unique_lock<mutex> ul(m);
      cv.wait_for(ul, rel_time, [&] { return !ok || xferFinish; });
      waitForLoad = false;
    }
  }

  void waitUntilNodesUpdate();

  template<class Rep, class Period>
  void waitUntilNodesUpdateFor(const std::chrono::duration<Rep, Period> &rel_time) {
    if (ok && loggedIn && !nodesUpdate && !waitForNewNodes) {
      waitForNewNodes = true;
      unique_lock<mutex> ul(m);
      cv.wait_for(ul, rel_time, [&] { return !ok || nodesUpdate; });
      waitForNewNodes = false;
    }
  }

  void waitUntilUnlink();

  template<class Rep, class Period>
  void waitUntilUnlinkFor(const std::chrono::duration<Rep, Period> &rel_time) {
    if (ok && loggedIn && !unlinked && !waitForUnlink) {
      waitForUnlink = true;
      unique_lock<mutex> ul(m);
      cv.wait_for(ul, rel_time, [&] { return !ok || unlinked; });
      waitForUnlink = false;
    }
  }

  double getLoginProgress();

  double getLoadProgress();

  void request_error(error error1) override;

  void request_response_progress(m_off_t off, m_off_t m_off) override;

  void prelogin_result(int i, string *string1, string *string2, error error1) override;

  void login_result(error error1) override;

  void logout_result(error error1) override;

  void userdata_result(string *string1, string *string2, string *string3, handle handle1, error error1) override;

  void pubkey_result(User *user) override;

  void ephemeral_result(error error1) override;

  void ephemeral_result(handle handle1, const Byte *byte1) override;

  void whyamiblocked_result(int i) override;

  void sendsignuplink_result(error error1) override;

  void querysignuplink_result(error error1) override;

  void querysignuplink_result(handle handle1, const char *string1, const char *string2, const Byte *byte1,
                              const Byte *byte2, const Byte *byte3, size_t size1) override;

  void confirmsignuplink_result(error error1) override;

  void confirmsignuplink2_result(handle handle1, const char *string1, const char *string2, error error1) override;

  void setkeypair_result(error error1) override;

  void account_details(AccountDetails *details, bool b, bool b1, bool b2, bool b3, bool b4, bool b5) override;

  void account_details(AccountDetails *details, error error1) override;

  void querytransferquota_result(int i) override;

  void sessions_killed(handle handle1, error error1) override;

  void setattr_result(handle handle1, error error1) override;

  void rename_result(handle handle1, error error1) override;

  void unlink_result(handle handle1, error error1) override;

  void unlinkversions_result(error error1) override;

  void nodes_updated(Node **pNode, int i) override;

  void pcrs_updated(PendingContactRequest **pRequest, int i) override;

  void users_updated(User **pUser, int i) override;

  void useralerts_updated(UserAlert::Base **pBase, int i) override;

  void account_updated() override;

  void changepw_result(error error1) override;

  void userattr_update(User *user, int i, const char *string1) override;

  void fetchnodes_result(error error1) override;

  void nodes_current() override;

  void putnodes_result(error error1, targettype_t targettype, NewNode *node) override;

  void share_result(error error1) override;

  void share_result(int i, error error1) override;

  void setpcr_result(handle handle1, error error1, opcactions_t opcactions) override;

  void updatepcr_result(error error1, ipcactions_t ipcactions) override;

  void fa_complete(handle handle1, fatype fatype1, const char *string1, uint32_t uint32) override;

  int fa_failed(handle handle1, fatype fatype1, int i, error error1) override;

  void putfa_result(handle handle1, fatype fatype1, error error1) override;

  void putfa_result(handle handle1, fatype fatype1, const char *string1) override;

  void enumeratequotaitems_result(handle handle1, unsigned int i, unsigned int i1, unsigned int i2, unsigned int i3,
                                  unsigned int i4, const char *string1, const char *string2, const char *string3,
                                  const char *string4) override;

  void enumeratequotaitems_result(error error1) override;

  void additem_result(error error1) override;

  void checkout_result(const char *string1, error error1) override;

  void submitpurchasereceipt_result(error error1) override;

  void creditcardstore_result(error error1) override;

  void creditcardquerysubscriptions_result(int i, error error1) override;

  void creditcardcancelsubscriptions_result(error error1) override;

  void getpaymentmethods_result(int i, error error1) override;

  void copysession_result(string *string1, error error1) override;

  void userfeedbackstore_result(error error1) override;

  void sendevent_result(error error1) override;

  void removecontact_result(error error1) override;

  void putua_result(error error1) override;

  void getua_result(error error1) override;

  void getua_result(Byte *byte1, unsigned int i) override;

  void getua_result(TLVstore *vstore) override;

  void getuseremail_result(string *string1, error error1) override;

  void exportnode_result(error error1) override;

  void exportnode_result(handle handle1, handle handle2) override;

  void openfilelink_result(error error1) override;

  void openfilelink_result(handle handle1, const Byte *byte1, m_off_t off, string *string1, string *string2,
                           int i) override;

  void checkfile_result(handle handle1, error error1) override;

  void checkfile_result(handle handle1, error error1, Byte *byte1, m_off_t off, m_time_t mTime, m_time_t time1,
                        string *string1, string *string2, string *string3) override;

  dstime pread_failure(error error1, int i, void *pVoid, dstime dstime1) override;

  bool pread_data(Byte *byte1, m_off_t off, m_off_t m_off, m_off_t mOff, m_off_t off1, void *pVoid) override;

  void reportevent_result(error error1) override;

  void cleanrubbishbin_result(error error1) override;

  void getrecoverylink_result(error error1) override;

  void queryrecoverylink_result(error error1) override;

  void queryrecoverylink_result(int i, const char *string1, const char *string2, time_t time1, handle handle1,
                                const vector<string> *vector1) override;

  void getprivatekey_result(error error1, const Byte *byte1, const size_t size1) override;

  void confirmrecoverylink_result(error error1) override;

  void confirmcancellink_result(error error1) override;

  void validatepassword_result(error error1) override;

  void getemaillink_result(error error1) override;

  void confirmemaillink_result(error error1) override;

  void getversion_result(int i, const char *string1, error error1) override;

  void getlocalsslcertificate_result(m_time_t mTime, string *string1, error error1) override;

  void getmegaachievements_result(AchievementsDetails *details, error error1) override;

  void getwelcomepdf_result(handle handle1, string *string1, error error1) override;

  void file_added(File *file) override;

  void file_removed(File *file, error error1) override;

  void file_complete(File *file) override;

  File *file_resume(string *string1, direction_t *direction) override;

  void transfer_added(Transfer *transfer) override;

  void transfer_removed(Transfer *transfer) override;

  void transfer_prepare(Transfer *transfer) override;

  void transfer_failed(Transfer *transfer, error error1, dstime dstime1) override;

  void transfer_update(Transfer *transfer) override;

  void transfer_complete(Transfer *transfer) override;

  void syncupdate_state(Sync *sync1, syncstate_t syncstate) override;

  void syncupdate_scanning(bool b) override;

  void syncupdate_local_folder_addition(Sync *sync1, LocalNode *node, const char *string1) override;

  void syncupdate_local_folder_deletion(Sync *sync1, LocalNode *node) override;

  void syncupdate_local_file_addition(Sync *sync1, LocalNode *node, const char *string1) override;

  void syncupdate_local_file_deletion(Sync *sync1, LocalNode *node) override;

  void syncupdate_local_file_change(Sync *sync1, LocalNode *node, const char *string1) override;

  void syncupdate_local_move(Sync *sync1, LocalNode *node, const char *string1) override;

  void syncupdate_local_lockretry(bool b) override;

  void syncupdate_get(Sync *sync1, Node *node, const char *string1) override;

  void syncupdate_put(Sync *sync1, LocalNode *node, const char *string1) override;

  void syncupdate_remote_file_addition(Sync *sync1, Node *node) override;

  void syncupdate_remote_file_deletion(Sync *sync1, Node *node) override;

  void syncupdate_remote_folder_addition(Sync *sync1, Node *node) override;

  void syncupdate_remote_folder_deletion(Sync *sync1, Node *node) override;

  void syncupdate_remote_copy(Sync *sync1, const char *string1) override;

  void syncupdate_remote_move(Sync *sync1, Node *node, Node *node1) override;

  void syncupdate_remote_rename(Sync *sync1, Node *node, const char *string1) override;

  void syncupdate_treestate(LocalNode *node) override;

  bool sync_syncable(Sync *sync1, const char *string1, string *string2, Node *node) override;

  bool sync_syncable(Sync *sync1, const char *string1, string *string2) override;

  void reload(const char *string1) override;

  void clearing() override;

  void notify_retry(dstime dstime1, retryreason_t retryreason) override;

  void notify_dbcommit() override;

  void notify_storage(int i) override;

  void notify_change_to_https() override;

  void notify_confirmation(const char *string1) override;

  void notify_disconnect() override;

  void http_result(error error1, int i, Byte *byte1, int i1) override;

  void timer_result(error error1) override;

  void contactlinkcreate_result(error error1, handle handle1) override;

  void contactlinkquery_result(error error1, handle handle1, string *string1, string *string2, string *string3,
                               string *string4) override;

  void contactlinkdelete_result(error error1) override;

  void multifactorauthsetup_result(string *string1, error error1) override;

  void multifactorauthcheck_result(int i) override;

  void multifactorauthdisable_result(error error1) override;

  void fetchtimezone_result(error error1, vector<string> *vector1, vector<int> *vector2, int i) override;

  void keepmealive_result(error error1) override;

  void getpsa_result(error error1, int i, string *string1, string *string2, string *string3, string *string4,
                     string *string5) override;

  void acknowledgeuseralerts_result(error error1) override;
};


#endif //LISTTEST_HELPERAPP_H
