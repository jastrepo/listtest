//
// Created by nikita on 2/16/19.
//

#include "HelperApp.h"
string errorString(error e) {
  switch (e) {
    case API_OK:
      return "No error";
    case API_EINTERNAL:
      return "Internal error";
    case API_EARGS:
      return "Invalid argument";
    case API_EAGAIN:
      return "Request failed, retrying";
    case API_ERATELIMIT:
      return "Rate limit exceeded";
    case API_EFAILED:
      return "Transfer failed";
    case API_ETOOMANY:
      return "Too many concurrent connections or transfers";
    case API_ERANGE:
      return "Out of range";
    case API_EEXPIRED:
      return "Expired";
    case API_ENOENT:
      return "Not found";
    case API_ECIRCULAR:
      return "Circular linkage detected";
    case API_EACCESS:
      return "Access denied";
    case API_EEXIST:
      return "Already exists";
    case API_EINCOMPLETE:
      return "Incomplete";
    case API_EKEY:
      return "Invalid key/integrity check failed";
    case API_ESID:
      return "Bad session ID";
    case API_EBLOCKED:
      return "Blocked";
    case API_EOVERQUOTA:
      return "Over quota";
    case API_ETEMPUNAVAIL:
      return "Temporarily not available";
    case API_ETOOMANYCONNECTIONS:
      return "Connection overflow";
    case API_EWRITE:
      return "Write error";
    case API_EREAD:
      return "Read error";
    case API_EAPPKEY:
      return "Invalid application key";
    case API_ESSL:
      return "SSL verification failed";
    case API_EGOINGOVERQUOTA:
      return "Not enough quota";
    case API_EMFAREQUIRED:
      return "Required 2FA pin";
    default:
      return "Unknown error";
  }
}

void HelperApp::setError(error newErrorType) {
  if (newErrorType != API_OK) {
    {
      lock_guard<mutex> lg(m);
      ok = false;
      errorType = newErrorType;
    }
    cv.notify_all();
  }
}

void HelperApp::setInternal() {
  {
    lock_guard<mutex> lg(m);
    internalError = true;
    ok = false;
  }
  cv.notify_all();
}

HelperApp::HelperApp() : ok(true), loggedIn(false), internalError(false), xferFinish(true),
                         nodesUpdate(true), unlinked(true), waitForLogin(false), waitForLoad(false),
                         waitForNewNodes(false), waitForUnlink(false), errorType(API_OK),
                         loginProgress(-1),
                         loadProgress(-1) {}

void HelperApp::logIn(const string &n_email, const string &n_password) {
  if (ok && !loggedIn && !waitForLogin) {
    email = n_email;
    password = n_password;
    client->prelogin(email.c_str());
  }
}

void HelperApp::putFile(File *file) {
  if (ok && loggedIn && !waitForLoad && !waitForNewNodes) {
    nodesUpdate = xferFinish = false;
    client->startxfer(PUT, file);
  }
}

void HelperApp::getFile(File *file) {
  if (ok && loggedIn && !waitForLoad) {
    xferFinish = false;
    client->startxfer(GET, file);
  }
}

void HelperApp::putNodes(handle targetDir, NewNode *nodes, int number) {
  if (ok && loggedIn && !waitForNewNodes) {
    nodesUpdate = false;
    client->putnodes(targetDir, nodes, number);
  }
}

void HelperApp::removeNode(Node *node) {
  if (ok && loggedIn && !waitForUnlink && !waitForNewNodes) {
    nodesUpdate = unlinked = false;
    setError(client->unlink(node));
  }
}


bool HelperApp::isOk() {
  return ok;
}

bool HelperApp::isLoggedIn() {
  return loggedIn;
}

bool HelperApp::isTransferFinished() {
  return xferFinish;
}

string HelperApp::getError() {
  if (internalError) {
    return "Internal error";
  } else {
    return errorString(errorType);
  }
}

void HelperApp::waitUntilLogin() {
  if (ok && !loggedIn && !waitForLogin) {
    waitForLogin = true;
    while (ok && !loggedIn) {
      unique_lock<mutex> ul(m);
      cv.wait(ul, [&] { return !ok || loggedIn; });
    }
    waitForLogin = false;
  }
}

void HelperApp::waitUntilLoad() {
  if (ok && loggedIn && !xferFinish && !waitForLoad) {
    waitForLoad = true;
    while (ok && !xferFinish) {
      unique_lock<mutex> ul(m);
      cv.wait(ul, [&] { return !ok || xferFinish; });
    }
    waitForLoad = false;
  }
}

void HelperApp::waitUntilNodesUpdate() {
  if (ok && loggedIn && !nodesUpdate && !waitForNewNodes) {
    waitForNewNodes = true;
    while (ok && !nodesUpdate) {
      unique_lock<mutex> ul(m);
      cv.wait(ul, [&] { return !ok || nodesUpdate; });
    }
    waitForNewNodes = false;
  }
}

void HelperApp::waitUntilUnlink() {
  if (ok && loggedIn && !unlinked && !waitForUnlink) {
    waitForUnlink = true;
    while (ok && !unlinked) {
      unique_lock<mutex> ul(m);
      cv.wait(ul, [&] { return !ok || unlinked; });
    }
    waitForUnlink = false;
  }
}

double HelperApp::getLoginProgress() {
  return loginProgress;
}

double HelperApp::getLoadProgress() {
  return loadProgress;
}

void HelperApp::request_error(error error1) {
  setError(error1);
  cout << "some callback:   request_error(error error1);" << endl;
}

void HelperApp::request_response_progress(m_off_t current, m_off_t total) {
  if (total > 0 && current >= 0 && loginProgress > 0) {
    loginProgress = 0.1 + current * 0.9 / total;
  }
}

void HelperApp::prelogin_result(int version, string *n_email, string *salt, error e) {
  if (e == API_OK) {
    Byte pwkey[SymmCipher::KEYLENGTH];
    if (email != *n_email) {
      setInternal();
    } else {
      if (version == 1) {
        error e1 = client->pw_key(password.c_str(), pwkey);
        if (e1 == API_OK) {
          client->login(email.c_str(), pwkey);
          loginProgress = 0.05;
        } else {
          setError(e1);
        }
      } else if (version == 2) {
        string m_salt = (salt != nullptr ? *salt : string());
        if (!m_salt.empty()) {
          client->login2(email.c_str(), password.c_str(), &m_salt, "");
          loginProgress = 0.05;
        } else {
          setInternal();
        }
      }
    }
  } else {
    setError(e);
  }
}

void HelperApp::login_result(error e) {
  if (e == API_OK) {
    loginProgress = 0.1;
    client->fetchnodes();
  } else {
    setError(e);
  }
}

void HelperApp::logout_result(error error1) {
  if (error1 == API_OK) {
    loggedIn = false;
  } else {
    setError(error1);
  }
  cout << "some callback:   logout_result(error1);" << endl;
}

void HelperApp::userdata_result(string *string1, string *string2, string *string3, handle handle1,
                                error error1) {
  setError(error1);
  MegaApp::userdata_result(string1, string2, string3, handle1, error1);
}

void HelperApp::pubkey_result(User *user) {
  cout << "some callback:   MegaApp::pubkey_result(user);" << endl;
  MegaApp::pubkey_result(user);
}

void HelperApp::ephemeral_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::ephemeral_result(error1);" << endl;
  MegaApp::ephemeral_result(error1);
}

void HelperApp::ephemeral_result(handle handle1, const Byte *byte1) {
  cout << "some callback:   MegaApp::ephemeral_result(handle1, byte1);" << endl;
  MegaApp::ephemeral_result(handle1, byte1);
}

void HelperApp::whyamiblocked_result(int i) {
  cout << "some callback:   MegaApp::whyamiblocked_result(i);" << endl;
  MegaApp::whyamiblocked_result(i);
}

void HelperApp::sendsignuplink_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::sendsignuplink_result(error1);" << endl;
  MegaApp::sendsignuplink_result(error1);
}

void HelperApp::querysignuplink_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::querysignuplink_result(error1);" << endl;
  MegaApp::querysignuplink_result(error1);
}

void HelperApp::querysignuplink_result(handle handle1, const char *string1, const char *string2,
                                       const Byte *byte1, const Byte *byte2, const Byte *byte3,
                                       size_t size) {
  cout << "some callback:   MegaApp::querysignuplink_result(handle1, string1, string2, byte1, byte2, byte3, size);"
       << endl;
  MegaApp::querysignuplink_result(handle1, string1, string2, byte1, byte2, byte3, size);
}

void HelperApp::confirmsignuplink_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::confirmsignuplink_result(error1);" << endl;
  MegaApp::confirmsignuplink_result(error1);
}

void HelperApp::confirmsignuplink2_result(handle handle1, const char *string1, const char *string2,
                                          error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::confirmsignuplink2_result(handle1, string1, string2, error1);" << endl;
  MegaApp::confirmsignuplink2_result(handle1, string1, string2, error1);
}

void HelperApp::setkeypair_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::setkeypair_result(error1);" << endl;
  MegaApp::setkeypair_result(error1);
}

void HelperApp::account_details(AccountDetails *details, bool b, bool b1, bool b2, bool b3, bool b4,
                                bool b5) {
  cout << "some callback:   MegaApp::account_details(details, b, b1, b2, b3, b4, b5);" << endl;
  MegaApp::account_details(details, b, b1, b2, b3, b4, b5);
}

void HelperApp::account_details(AccountDetails *details, error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::account_details(details, error1);" << endl;
  MegaApp::account_details(details, error1);
}

void HelperApp::querytransferquota_result(int i) {
  cout << "some callback:   MegaApp::querytransferquota_result(i);" << endl;
  MegaApp::querytransferquota_result(i);
}

void HelperApp::sessions_killed(handle handle1, error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::sessions_killed(handle1, error1);" << endl;
  MegaApp::sessions_killed(handle1, error1);
}

void HelperApp::setattr_result(handle handle1, error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::setattr_result(handle1, error1);" << endl;
  MegaApp::setattr_result(handle1, error1);
}

void HelperApp::rename_result(handle handle1, error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::rename_result(handle1, error1);" << endl;
  MegaApp::rename_result(handle1, error1);
}

void HelperApp::unlink_result(handle handle1, error error1) {
  if (error1 == API_OK) {
    {
      lock_guard<mutex> lg(m);
      unlinked = true;
    }
    cv.notify_all();
  } else {
    setError(error1);
  }
  cout << "some callback:   MegaApp::unlink_result(handle1, error1);" << endl;
  MegaApp::unlink_result(handle1, error1);
}

void HelperApp::unlinkversions_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::unlinkversions_result(error1);" << endl;
  MegaApp::unlinkversions_result(error1);
}

void HelperApp::nodes_updated(Node **pNode, int i) {
  {
    lock_guard<mutex> lg(m);
    nodesUpdate = true;
  }
  cv.notify_all();
  MegaApp::nodes_updated(pNode, i);
}

void HelperApp::pcrs_updated(PendingContactRequest **pRequest, int i) {
  if (i != 0) {
    cout << "some callback:   MegaApp::pcrs_updated(pRequest, " << i << ");" << endl;
  }
  MegaApp::pcrs_updated(pRequest, i);
}

void HelperApp::users_updated(User **pUser, int i) {
  MegaApp::users_updated(pUser, i);
}

void HelperApp::useralerts_updated(UserAlert::Base **pBase, int i) {
  if (i != 0) {
    cout << "some callback:   MegaApp::useralerts_updated(pBase, " << i << ");" << endl;
  }
  MegaApp::useralerts_updated(pBase, i);
}

void HelperApp::account_updated() {
  cout << "some callback:   MegaApp::account_updated();" << endl;
  MegaApp::account_updated();
}

void HelperApp::changepw_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::changepw_result(error1);" << endl;
  MegaApp::changepw_result(error1);
}

void HelperApp::userattr_update(User *user, int i, const char *string1) {
  cout << "some callback:   MegaApp::userattr_update(user, i, string1);" << endl;
  MegaApp::userattr_update(user, i, string1);
}

void HelperApp::fetchnodes_result(error e) {
  if (e == API_OK) {
    loginProgress = 1;
    {
      lock_guard<mutex> lg(m);
      loggedIn = true;
    }
    cv.notify_all();
  } else {
    setError(e);
  }
}

void HelperApp::nodes_current() {
  MegaApp::nodes_current();
}

void HelperApp::putnodes_result(error error1, targettype_t targettype, NewNode *node) {
  setError(error1);
  MegaApp::putnodes_result(error1, targettype, node);
}

void HelperApp::share_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::share_result(error1);" << endl;
  MegaApp::share_result(error1);
}

void HelperApp::share_result(int i, error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::share_result(i, error1);" << endl;
  MegaApp::share_result(i, error1);
}

void HelperApp::setpcr_result(handle handle1, error error1, opcactions_t opcactions) {
  setError(error1);
  cout << "some callback:   MegaApp::setpcr_result(handle1, error1, opcactions);" << endl;
  MegaApp::setpcr_result(handle1, error1, opcactions);
}

void HelperApp::updatepcr_result(error error1, ipcactions_t ipcactions) {
  setError(error1);
  cout << "some callback:   MegaApp::updatepcr_result(error1, ipcactions);" << endl;
  MegaApp::updatepcr_result(error1, ipcactions);
}

void HelperApp::fa_complete(handle handle1, fatype fatype1, const char *string1,
                            uint32_t uint32) {
  cout << "some callback:   MegaApp::fa_complete(handle1, fatype1, string1, uint32);" << endl;
  MegaApp::fa_complete(handle1, fatype1, string1, uint32);
}

int HelperApp::fa_failed(handle handle1, fatype fatype1, int i, error error1) {
  setError(error1);
  return MegaApp::fa_failed(handle1, fatype1, i, error1);
}

void HelperApp::putfa_result(handle handle1, fatype fatype1, error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::putfa_result(handle1, fatype1, error1);" << endl;
  MegaApp::putfa_result(handle1, fatype1, error1);
}

void HelperApp::putfa_result(handle handle1, fatype fatype1, const char *string1) {
  cout << "some callback:   MegaApp::putfa_result(handle1, fatype1, string1);" << endl;
  MegaApp::putfa_result(handle1, fatype1, string1);
}

void
HelperApp::enumeratequotaitems_result(handle handle1, unsigned int i, unsigned int i1, unsigned int i2,
                                      unsigned int i3, unsigned int i4, const char *string1,
                                      const char *string2, const char *string3, const char *string4) {
  cout
      << "some callback:   MegaApp::enumeratequotaitems_result(handle1, i, i1, i2, i3, i4, string1, string2, string3, string4);"
      << endl;
  MegaApp::enumeratequotaitems_result(handle1, i, i1, i2, i3, i4, string1, string2, string3, string4);
}

void HelperApp::enumeratequotaitems_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::enumeratequotaitems_result(error1);" << endl;
  MegaApp::enumeratequotaitems_result(error1);
}

void HelperApp::additem_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::additem_result(error1);" << endl;
  MegaApp::additem_result(error1);
}

void HelperApp::checkout_result(const char *string1, error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::checkout_result(string1, error1);" << endl;
  MegaApp::checkout_result(string1, error1);
}

void HelperApp::submitpurchasereceipt_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::submitpurchasereceipt_result(error1);" << endl;
  MegaApp::submitpurchasereceipt_result(error1);
}

void HelperApp::creditcardstore_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::creditcardstore_result(error1);" << endl;
  MegaApp::creditcardstore_result(error1);
}

void HelperApp::creditcardquerysubscriptions_result(int i, error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::creditcardquerysubscriptions_result(i, error1);" << endl;
  MegaApp::creditcardquerysubscriptions_result(i, error1);
}

void HelperApp::creditcardcancelsubscriptions_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::creditcardcancelsubscriptions_result(error1);" << endl;
  MegaApp::creditcardcancelsubscriptions_result(error1);
}

void HelperApp::getpaymentmethods_result(int i, error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::getpaymentmethods_result(i, error1);" << endl;
  MegaApp::getpaymentmethods_result(i, error1);
}

void HelperApp::copysession_result(string *string1, error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::copysession_result(string1, error1);" << endl;
  MegaApp::copysession_result(string1, error1);
}

void HelperApp::userfeedbackstore_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::userfeedbackstore_result(error1);" << endl;
  MegaApp::userfeedbackstore_result(error1);
}

void HelperApp::sendevent_result(error error1) {
  setError(error1);
  MegaApp::sendevent_result(error1);
}

void HelperApp::removecontact_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::removecontact_result(error1);" << endl;
  MegaApp::removecontact_result(error1);
}

void HelperApp::putua_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::putua_result(error1);" << endl;
  MegaApp::putua_result(error1);
}

void HelperApp::getua_result(error error1) {
  //setError(error1);
  MegaApp::getua_result(error1);
}

void HelperApp::getua_result(Byte *byte1, unsigned int i) {
  cout << "some callback:   MegaApp::getua_result(byte1, i);" << endl;
  MegaApp::getua_result(byte1, i);
}

void HelperApp::getua_result(TLVstore *vstore) {
  cout << "some callback:   MegaApp::getua_result(vstore);" << endl;
  MegaApp::getua_result(vstore);
}

void HelperApp::getuseremail_result(string *string1, error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::getuseremail_result(string1, error1);" << endl;
  MegaApp::getuseremail_result(string1, error1);
}

void HelperApp::exportnode_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::exportnode_result(error1);" << endl;
  MegaApp::exportnode_result(error1);
}

void HelperApp::exportnode_result(handle handle1, handle handle2) {
  cout << "some callback:   MegaApp::exportnode_result(handle1, handle2);" << endl;
  MegaApp::exportnode_result(handle1, handle2);
}

void HelperApp::openfilelink_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::openfilelink_result(error1);" << endl;
  MegaApp::openfilelink_result(error1);
}

void HelperApp::openfilelink_result(handle handle1, const Byte *byte1, m_off_t off, string *string1,
                                    string *string2, int i) {
  cout << "some callback:   MegaApp::openfilelink_result(handle1, byte1, off, string1, string2, i);" << endl;
  MegaApp::openfilelink_result(handle1, byte1, off, string1, string2, i);
}

void HelperApp::checkfile_result(handle handle1, error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::checkfile_result(handle1, error1);" << endl;
  MegaApp::checkfile_result(handle1, error1);
}

void HelperApp::checkfile_result(handle handle1, error error1, Byte *byte1, m_off_t off, m_time_t mTime,
                                 m_time_t time1, string *string1, string *string2, string *string3) {
  setError(error1);
  cout
      << "some callback:   MegaApp::checkfile_result(handle1, error1, byte1, off, mTime, time1, string1, string2, string3);"
      << endl;
  MegaApp::checkfile_result(handle1, error1, byte1, off, mTime, time1, string1, string2, string3);
}

dstime HelperApp::pread_failure(error error1, int i, void *pVoid,
                                dstime dstime1) {
  setError(error1);
  return MegaApp::pread_failure(error1, i, pVoid, dstime1);
}

bool HelperApp::pread_data(Byte *byte1, m_off_t off, m_off_t m_off, m_off_t mOff, m_off_t off1,
                           void *pVoid) {
  return MegaApp::pread_data(byte1, off, m_off, mOff, off1, pVoid);
}

void HelperApp::reportevent_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::reportevent_result(error1);" << endl;
  MegaApp::reportevent_result(error1);
}

void HelperApp::cleanrubbishbin_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::cleanrubbishbin_result(error1);" << endl;
  MegaApp::cleanrubbishbin_result(error1);
}

void HelperApp::getrecoverylink_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::getrecoverylink_result(error1);" << endl;
  MegaApp::getrecoverylink_result(error1);
}

void HelperApp::queryrecoverylink_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::queryrecoverylink_result(error1);" << endl;
  MegaApp::queryrecoverylink_result(error1);
}

void HelperApp::queryrecoverylink_result(int i, const char *string1, const char *string2, time_t time1,
                                         handle handle1, const vector<string> *vector1) {
  cout << "some callback:   MegaApp::queryrecoverylink_result(i, string1, string2, time1, handle1, vector1);"
       << endl;
  MegaApp::queryrecoverylink_result(i, string1, string2, time1, handle1, vector1);
}

void HelperApp::getprivatekey_result(error error1, const Byte *byte1, const size_t size) {
  setError(error1);
  cout << "some callback:   MegaApp::getprivatekey_result(error1, byte1, size);" << endl;
  MegaApp::getprivatekey_result(error1, byte1, size);
}

void HelperApp::confirmrecoverylink_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::confirmrecoverylink_result(error1);" << endl;
  MegaApp::confirmrecoverylink_result(error1);
}

void HelperApp::confirmcancellink_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::confirmcancellink_result(error1);" << endl;
  MegaApp::confirmcancellink_result(error1);
}

void HelperApp::validatepassword_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::validatepassword_result(error1);" << endl;
  MegaApp::validatepassword_result(error1);
}

void HelperApp::getemaillink_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::getemaillink_result(error1);" << endl;
  MegaApp::getemaillink_result(error1);
}

void HelperApp::confirmemaillink_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::confirmemaillink_result(error1);" << endl;
  MegaApp::confirmemaillink_result(error1);
}

void HelperApp::getversion_result(int i, const char *string1, error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::getversion_result(i, string1, error1);" << endl;
  MegaApp::getversion_result(i, string1, error1);
}

void HelperApp::getlocalsslcertificate_result(m_time_t mTime, string *string1, error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::getlocalsslcertificate_result(mTime, string1, error1);" << endl;
  MegaApp::getlocalsslcertificate_result(mTime, string1, error1);
}

void HelperApp::getmegaachievements_result(AchievementsDetails *details, error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::getmegaachievements_result(details, error1);" << endl;
  MegaApp::getmegaachievements_result(details, error1);
}

void HelperApp::getwelcomepdf_result(handle handle1, string *string1, error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::getwelcomepdf_result(handle1, string1, error1);" << endl;
  MegaApp::getwelcomepdf_result(handle1, string1, error1);
}

void HelperApp::file_added(File *file) {
  MegaApp::file_added(file);
}

void HelperApp::file_removed(File *file, error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::file_removed(file, error1);" << endl;
  MegaApp::file_removed(file, error1);
}

void HelperApp::file_complete(File *file) {
  MegaApp::file_complete(file);
}

File *HelperApp::file_resume(string *string1, direction_t *direction) {
  return MegaApp::file_resume(string1, direction);
}

void HelperApp::transfer_added(Transfer *transfer) {
  xferFinish = false;
  loadProgress = -1;
  MegaApp::transfer_added(transfer);
}

void HelperApp::transfer_removed(Transfer *transfer) {
  loadProgress = -1;
  {
    lock_guard<mutex> lg(m);
    xferFinish = true;
  }
  setInternal();
  cout << "some callback:   MegaApp::transfer_removed(transfer);" << endl;
  MegaApp::transfer_removed(transfer);
}

void HelperApp::transfer_prepare(Transfer *transfer) {
  xferFinish = false;
  loadProgress = 0;
  MegaApp::transfer_prepare(transfer);
}

void HelperApp::transfer_failed(Transfer *transfer, error error1, dstime dstime1) {
  loadProgress = -1;
  {
    lock_guard<mutex> lg(m);
    xferFinish = true;
  }
  setError(error1);
  cout << "some callback:   MegaApp::transfer_failed(transfer, error1, dstime1);" << endl;
  MegaApp::transfer_failed(transfer, error1, dstime1);
}

void HelperApp::transfer_update(Transfer *transfer) {
  if (transfer->slot->progressreported >= 0 && transfer->size > 0) {
    loadProgress = double(transfer->slot->progressreported) / transfer->size;
  }
  MegaApp::transfer_update(transfer);
}

void HelperApp::transfer_complete(Transfer *transfer) {
  loadProgress = 1;
  {
    lock_guard<mutex> lg(m);
    xferFinish = true;
  }
  cv.notify_all();
  MegaApp::transfer_complete(transfer);
}

void HelperApp::syncupdate_state(Sync *sync1, syncstate_t syncstate) {
  cout << "some callback:   MegaApp::syncupdate_state(sync1, syncstate);" << endl;
  MegaApp::syncupdate_state(sync1, syncstate);
}

void HelperApp::syncupdate_scanning(bool b) {
  cout << "some callback:   MegaApp::syncupdate_scanning(b);" << endl;
  MegaApp::syncupdate_scanning(b);
}

void HelperApp::syncupdate_local_folder_addition(Sync *sync1, LocalNode *node, const char *string1) {
  cout << "some callback:   MegaApp::syncupdate_local_folder_addition(sync1, node, string1);" << endl;
  MegaApp::syncupdate_local_folder_addition(sync1, node, string1);
}

void HelperApp::syncupdate_local_folder_deletion(Sync *sync1, LocalNode *node) {
  cout << "some callback:   MegaApp::syncupdate_local_folder_deletion(sync1, node);" << endl;
  MegaApp::syncupdate_local_folder_deletion(sync1, node);
}

void HelperApp::syncupdate_local_file_addition(Sync *sync1, LocalNode *node, const char *string1) {
  cout << "some callback:   MegaApp::syncupdate_local_file_addition(sync1, node, string1);" << endl;
  MegaApp::syncupdate_local_file_addition(sync1, node, string1);
}

void HelperApp::syncupdate_local_file_deletion(Sync *sync1, LocalNode *node) {
  cout << "some callback:   MegaApp::syncupdate_local_file_deletion(sync1, node);" << endl;
  MegaApp::syncupdate_local_file_deletion(sync1, node);
}

void HelperApp::syncupdate_local_file_change(Sync *sync1, LocalNode *node, const char *string1) {
  cout << "some callback:   MegaApp::syncupdate_local_file_change(sync1, node, string1);" << endl;
  MegaApp::syncupdate_local_file_change(sync1, node, string1);
}

void HelperApp::syncupdate_local_move(Sync *sync1, LocalNode *node, const char *string1) {
  cout << "some callback:   MegaApp::syncupdate_local_move(sync1, node, string1);" << endl;
  MegaApp::syncupdate_local_move(sync1, node, string1);
}

void HelperApp::syncupdate_local_lockretry(bool b) {
  cout << "some callback:   MegaApp::syncupdate_local_lockretry(b);" << endl;
  MegaApp::syncupdate_local_lockretry(b);
}

void HelperApp::syncupdate_get(Sync *sync1, Node *node, const char *string1) {
  cout << "some callback:   MegaApp::syncupdate_get(sync1, node, string1);" << endl;
  MegaApp::syncupdate_get(sync1, node, string1);
}

void HelperApp::syncupdate_put(Sync *sync1, LocalNode *node, const char *string1) {
  cout << "some callback:   MegaApp::syncupdate_put(sync1, node, string1);" << endl;
  MegaApp::syncupdate_put(sync1, node, string1);
}

void HelperApp::syncupdate_remote_file_addition(Sync *sync1, Node *node) {
  cout << "some callback:   MegaApp::syncupdate_remote_file_addition(sync1, node);" << endl;
  MegaApp::syncupdate_remote_file_addition(sync1, node);
}

void HelperApp::syncupdate_remote_file_deletion(Sync *sync1, Node *node) {
  cout << "some callback:   MegaApp::syncupdate_remote_file_deletion(sync1, node);" << endl;
  MegaApp::syncupdate_remote_file_deletion(sync1, node);
}

void HelperApp::syncupdate_remote_folder_addition(Sync *sync1, Node *node) {
  cout << "some callback:   MegaApp::syncupdate_remote_folder_addition(sync1, node);" << endl;
  MegaApp::syncupdate_remote_folder_addition(sync1, node);
}

void HelperApp::syncupdate_remote_folder_deletion(Sync *sync1, Node *node) {
  cout << "some callback:   MegaApp::syncupdate_remote_folder_deletion(sync1, node);" << endl;
  MegaApp::syncupdate_remote_folder_deletion(sync1, node);
}

void HelperApp::syncupdate_remote_copy(Sync *sync1, const char *string1) {
  cout << "some callback:   MegaApp::syncupdate_remote_copy(sync1, string1);" << endl;
  MegaApp::syncupdate_remote_copy(sync1, string1);
}

void HelperApp::syncupdate_remote_move(Sync *sync1, Node *node, Node *node1) {
  cout << "some callback:   MegaApp::syncupdate_remote_move(sync1, node, node1);" << endl;
  MegaApp::syncupdate_remote_move(sync1, node, node1);
}

void HelperApp::syncupdate_remote_rename(Sync *sync1, Node *node, const char *string1) {
  cout << "some callback:   MegaApp::syncupdate_remote_rename(sync1, node, string1);" << endl;
  MegaApp::syncupdate_remote_rename(sync1, node, string1);
}

void HelperApp::syncupdate_treestate(LocalNode *node) {
  cout << "some callback:   MegaApp::syncupdate_treestate(node);" << endl;
  MegaApp::syncupdate_treestate(node);
}

bool HelperApp::sync_syncable(Sync *sync1, const char *string1, string *string2, Node *node) {
  return MegaApp::sync_syncable(sync1, string1, string2, node);
}

bool HelperApp::sync_syncable(Sync *sync1, const char *string1, string *string2) {
  return MegaApp::sync_syncable(sync1, string1, string2);
}

void HelperApp::reload(const char *string1) {
  cout << "some callback:   MegaApp::reload(string1);" << endl;
  MegaApp::reload(string1);
}

void HelperApp::clearing() {
  MegaApp::clearing();
}

void HelperApp::notify_retry(dstime dstime1, retryreason_t retryreason) {
  switch (retryreason) {
    case RETRY_NONE:
      cout << "RETRY_NONE";
      break;
    case RETRY_CONNECTIVITY:
      cout << "RETRY_CONNECTIVITY";
      break;
    case RETRY_SERVERS_BUSY:
      cout << "RETRY_SERVERS_BUSY";
      break;
    case RETRY_API_LOCK:
      cout << "RETRY_API_LOCK";
      break;
    case RETRY_RATE_LIMIT:
      cout << "RETRY_RATE_LIMIT";
      break;
    case RETRY_LOCAL_LOCK:
      cout << "RETRY_LOCAL_LOCK";
      break;
    case RETRY_UNKNOWN:
      cout << "RETRY_UNKNOWN";
      break;
  }
  cout << " " << dstime1 << endl;
  cout << "some callback:   MegaApp::notify_retry(dstime1, retryreason);" << endl;
  MegaApp::notify_retry(dstime1, retryreason);
}

void HelperApp::notify_dbcommit() {
  MegaApp::notify_dbcommit();
}

void HelperApp::notify_storage(int i) {
  cout << "some callback:   MegaApp::notify_storage(i);" << endl;
  MegaApp::notify_storage(i);
}

void HelperApp::notify_change_to_https() {
  cout << "some callback:   MegaApp::notify_change_to_https();" << endl;
  MegaApp::notify_change_to_https();
}

void HelperApp::notify_confirmation(const char *string1) {
  cout << "some callback:   MegaApp::notify_confirmation(string1);" << endl;
  MegaApp::notify_confirmation(string1);
}

void HelperApp::notify_disconnect() {
  MegaApp::notify_disconnect();
}

void HelperApp::http_result(error error1, int i, Byte *byte1, int i1) {
  setError(error1);
  cout << "some callback:   MegaApp::http_result(error1, i, byte1, i1);" << endl;
  MegaApp::http_result(error1, i, byte1, i1);
}

void HelperApp::timer_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::timer_result(error1);" << endl;
  MegaApp::timer_result(error1);
}

void HelperApp::contactlinkcreate_result(error error1, handle handle1) {
  setError(error1);
  cout << "some callback:   MegaApp::contactlinkcreate_result(error1, handle1);" << endl;
  MegaApp::contactlinkcreate_result(error1, handle1);
}

void HelperApp::contactlinkquery_result(error error1, handle handle1, string *string1, string *string2,
                                        string *string3, string *string4) {
  setError(error1);
  cout << "some callback:   MegaApp::contactlinkquery_result(error1, handle1, string1, string2, string3, string4);"
       << endl;
  MegaApp::contactlinkquery_result(error1, handle1, string1, string2, string3, string4);
}

void HelperApp::contactlinkdelete_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::contactlinkdelete_result(error1);" << endl;
  MegaApp::contactlinkdelete_result(error1);
}

void HelperApp::multifactorauthsetup_result(string *string1, error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::multifactorauthsetup_result(string1, error1);" << endl;
  MegaApp::multifactorauthsetup_result(string1, error1);
}

void HelperApp::multifactorauthcheck_result(int i) {
  cout << "some callback:   MegaApp::multifactorauthcheck_result(i);" << endl;
  MegaApp::multifactorauthcheck_result(i);
}

void HelperApp::multifactorauthdisable_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::multifactorauthdisable_result(error1);" << endl;
  MegaApp::multifactorauthdisable_result(error1);
}

void HelperApp::fetchtimezone_result(error error1, vector<string> *vector1, vector<int> *vector2,
                                     int i) {
  setError(error1);
  cout << "some callback:   MegaApp::fetchtimezone_result(error1, vector1, vector2, i);" << endl;
  MegaApp::fetchtimezone_result(error1, vector1, vector2, i);
}

void HelperApp::keepmealive_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::keepmealive_result(error1);" << endl;
  MegaApp::keepmealive_result(error1);
}

void HelperApp::getpsa_result(error error1, int i, string *string1, string *string2, string *string3,
                              string *string4, string *string5) {
  setError(error1);
  cout << "some callback:   MegaApp::getpsa_result(error1, i, string1, string2, string3, string4, string5);"
       << endl;
  MegaApp::getpsa_result(error1, i, string1, string2, string3, string4, string5);
}

void HelperApp::acknowledgeuseralerts_result(error error1) {
  setError(error1);
  cout << "some callback:   MegaApp::acknowledgeuseralerts_result(error1);" << endl;
  MegaApp::acknowledgeuseralerts_result(error1);
}