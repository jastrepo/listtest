//
// Created by nikita on 3/8/19.
//

#include "Client.h"
Client::Client() : currTime(chrono::system_clock::now()) {
  wa = new CONSOLE_WAIT_CLASS;
  io = new HTTPIO_CLASS;
  fs = new FSACCESS_CLASS;
  gfx = new GFX_CLASS;
  app = new HelperApp;
  client = new MegaClient(app, wa, io, fs, new DBACCESS_CLASS, gfx, "gBkSEYSD", "");
  running = true;
  promise<void> promiseStop;
  stop = promiseStop.get_future();
  thread([&](promise<void> tStop) {
    this_thread::sleep_for(1s);
    while (running) {
      client->exec();
      client->wait();
#ifdef ENABLE_TEST
      currTime = chrono::system_clock::now();
#endif
    }
    tStop.set_value();
  }, move(promiseStop)).detach();
}

Client::~Client() {
  running = false;
  stop.get();
  if (client != nullptr) {
    client->removecaches();
    delete client;
  }
  delete app;
  delete wa;
  delete io;
  delete fs;
  delete gfx;
}

void Client::login(const string &email, const string &pass) {
  app->logIn(email, pass);
  app->waitUntilLogin();
  /*while (app->isOk() && !app->isLoggedIn()) {
    app->waitUntilLoginFor(1s);
    auto tim = chrono::system_clock::to_time_t(currTime);
    cout << "last time worked " << put_time(localtime(&tim), "%T") << " logged in to " << app->getLoginProgress()
         << endl;
  }*/
  if (!app->isOk()) {
    throw runtime_error(app->getError());
  }
}

HelperApp *Client::getApp() const {
  return app;
}

MegaClient *Client::getClient() const {
  return client;
}
