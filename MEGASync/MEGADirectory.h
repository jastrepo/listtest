//
// Created by nikita on 2/5/19.
//
#ifndef LISTTEST_MEGADIRECTORY_H
#define LISTTEST_MEGADIRECTORY_H
#include <filesystem>
#include "Client.h"

class MEGADirectory {
  Client *client;
  Node *remoteDir;
  filesystem::path localDir;
  bool remove = false;

  void removeDir();

public:
  class iterator {
    const Node *remoteDir;
    list<Node *>::const_iterator it;
  public:
    bool operator!=(const iterator &right) const;

    bool operator==(const iterator &right) const;

    const string operator*() const;

    iterator &operator++();

    iterator();

    explicit iterator(Node *remoteDir);
  };

  explicit MEGADirectory(Client *client);

  ~MEGADirectory();

  void setDirs(const string &localPath, const string &remotePath);

  double getLoginProgress() const;

  double getLoadProgress() const;

  void removeAfterDestruction(bool remove);

  void getFile(const string &name);

  void putFile(const string &name);

  void removeFile(const string &name);

  MEGADirectory::iterator begin() const;

  MEGADirectory::iterator end() const;
};

MEGADirectory::iterator begin(const MEGADirectory &dir);

MEGADirectory::iterator end(const MEGADirectory &dir);

#endif //LISTTEST_MEGADIRECTORY_H