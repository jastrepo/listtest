//
// Created by nikita on 3/8/19.
//

#ifndef LISTTEST_CLIENT_H
#define LISTTEST_CLIENT_H

#include <future>
#include "HelperApp.h"

class Client {
  atomic<bool> running;
  future<void> stop;
  atomic<chrono::time_point<chrono::system_clock>> currTime;
  MegaClient *client = nullptr;
  HelperApp *app = nullptr;
  CONSOLE_WAIT_CLASS *wa = nullptr;
  HTTPIO_CLASS *io = nullptr;
  FSACCESS_CLASS *fs = nullptr;
  GFX_CLASS *gfx = nullptr;
public:
  Client();

  void login(const string &email, const string &pass);

  ~Client();

  HelperApp *getApp() const;

  MegaClient *getClient() const;
};

#endif //LISTTEST_CLIENT_H
