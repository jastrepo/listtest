//
// Created by nikita on 3/8/19.
//
#include <mega.h>
#include "ListsManager.h"

shared_ptr<List> ListsManager::addList(const string &name, const string &password, bool newForceOpen, bool decrypt) {
  auto it = lists.find(name);
  if (it == lists.end()) {
    auto list = make_shared<List>();
    if (saving) {
      try {
        auto lockFileName = localDir / ("d_" + name);
        ifstream lockFile(lockFileName);
        lockFile.exceptions(ios::badbit);
        if (lockFile && !(forceOpen || newForceOpen)) {
          throw runtime_error("list \"" + name + "\" is already open");
        } else {
          ifstream listFile(localDir / ("l_" + name));
          listFile.exceptions(ios::badbit);
          if (listFile) {
            ifstream keyFile(localDir / ("k_" + name));
            keyFile.exceptions(ios::badbit);
            string keyStr;
            if (keyFile) {
              keyStr.append((istreambuf_iterator<char>(keyFile)), istreambuf_iterator<char>());
            }
            keyFile.close();
            if (keyStr.empty()) {
              if (decrypt) {
                throw logic_error("Trying to decrypt unencrypted list");
              }
            } else {
              if (!decrypt || password.empty()) {
                throw logic_error("Trying to read encrypted list");
              }
              list->openKey(password, keyStr);
            }
            list->loadFromStream(listFile);
          }
          listFile.close();
          ofstream newLockFile(lockFileName);
          newLockFile.exceptions(ios::badbit);
          newLockFile.close();
        }
        lockFile.close();
      } catch (ios_base::failure &e) {
        throw runtime_error("Error in addList: "s + e.what());
      }
    }
    return (*lists.emplace(name, list).first).second;
  } else {
    return (*it).second;
  }
}

void ListsManager::forceOpenLists(bool newForceOpen) {
  forceOpen = newForceOpen;
}

void ListsManager::setSavingDir(const filesystem::path &dir, bool clearDir) {
  try {
    if (!filesystem::exists(dir)) {
      filesystem::create_directories(dir);
      filesystem::create_directory(dir);
    }
    if (clearDir) {
      for (const auto &file:filesystem::directory_iterator(dir)) {
        if (file != dir) {
          filesystem::remove_all(file);
        }
      }
    }
    if (saving) {
      for (const auto &file:localDir) {
        filesystem::rename(file, dir / file.filename());
      }
    }
    localDir = dir;
    saving = true;
  } catch (filesystem::filesystem_error &e) {
    throw runtime_error("Error in setSavingDirectory: "s + e.what());
  }
}

void ListsManager::resetSavingDir() {
  saving = false;
}

shared_ptr<List> ListsManager::getList(const string &name) {
  return lists.at(name);
}

List &ListsManager::operator[](const string &name) {
  return *getList(name);
}

void ListsManager::saveAll() {
  if (saving) {
    for (auto it = lists.begin(); it != lists.end(); it++) {
      saveList(it);
    }
  }
}

void ListsManager::closeList(const string &name) {
  auto it = lists.find(name);
  if (it != lists.end()) {
    closeList(it, autoSave);
  }
}

ListsManager::~ListsManager() {
  for (auto it = lists.begin(); it != lists.end(); it++) {
    closeList(it, autoSave, false);
  }
}

void ListsManager::saveList(const string &name) {
  auto it = lists.find(name);
  if (it != lists.end()) {
    saveList(it);
  }
}

void ListsManager::saveList(ListsManager::listType::iterator it) {
  if (saving) {
    try {
      ofstream listFile(localDir / ("l_" + (*it).first));
      listFile.exceptions(ios::badbit);
      (*it).second->saveToStream(listFile);
    } catch (ios_base::failure &e) {
      throw runtime_error("Error in saveList: "s + e.what());
    }
  }
}

void ListsManager::closeList(ListsManager::listType::iterator it, bool save, bool removeFromList) {
  if (saving) {
    if (save) {
      saveList(it);
    }
    try {
      filesystem::remove(localDir / ("d_" + (*it).first));
    } catch (filesystem::filesystem_error &e) {
      throw runtime_error("Error in closeList"s + e.what());
    }
  }
  if (removeFromList) {
    lists.erase(it);
  }
}

void ListsManager::setPassword(const string &name, const string &password) {
  auto it = lists.find(name);
  if (it != lists.end()) {
    string keyStr = (*it).second->setPassword(password);
    if (saving) {
      try {
        ofstream keyFile(localDir / ("k_" + name));
        keyFile.exceptions(ios::badbit);
        keyFile << keyStr;
        keyFile.close();
      } catch (ios_base::failure &e) {
        throw runtime_error("Error in setPassword: "s + e.what());
      }
    }
  }
}

void ListsManager::setPassword(const string &name, const string &publicPassword, const string &privatePassword) {
  auto it = lists.find(name);
  if (it != lists.end()) {
    string keyStr = (*it).second->setPassword(publicPassword, privatePassword);
    if (saving) {
      try {
        ofstream keyFile(localDir / ("k_" + name));
        keyFile.exceptions(ios::badbit);
        keyFile << keyStr;
        keyFile.close();
      } catch (ios_base::failure &e) {
        throw runtime_error("Error in setPassword: "s + e.what());
      }
    }
  }
}

void ListsManager::removePassword(const string &name) {
  auto it = lists.find(name);
  if (it != lists.end()) {
    (*it).second->removePassword();
    if (saving) {
      try {
        filesystem::remove(localDir / ("k_" + name));
      } catch (filesystem::filesystem_error &e) {
        throw runtime_error("Error in closeList"s + e.what());
      }
    }
  }
}

shared_ptr<List> ListsManager::addList(const string &name, bool newForceOpen) {
  return addList(name, "", newForceOpen, false);
}

void ListsManager::autoSaving(bool newAutoSave) {
  autoSave = newAutoSave;
}

shared_ptr<List> ListsManager::addEncryptedList(const string &name, const string &password, bool newForceOpen) {
  return addList(name, password, newForceOpen, true);
}
