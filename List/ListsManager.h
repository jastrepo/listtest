//
// Created by nikita on 3/8/19.
//
#ifndef LISTTEST_LISTSMANAGER_H
#define LISTTEST_LISTSMANAGER_H

#include "List.h"

class MEGADirectory;

class ListsManager {
  using listType=map<string, shared_ptr<List>>;
  listType lists; // TODO: replace with smart pointers
  bool saving = false, autoSave = true, forceOpen = false;
  filesystem::path localDir;

  void saveList(listType::iterator it);

  void closeList(listType::iterator it, bool save, bool removeFromList = true);

  shared_ptr<List> addList(const string &name, const string &password, bool newForceOpen, bool decrypt);

public:

  ~ListsManager();

  void forceOpenLists(bool newForceOpen = true);

  shared_ptr<List> addEncryptedList(const string &name, const string &password, bool newForceOpen = false);

  shared_ptr<List> addList(const string &name, bool newForceOpen = false);

  void autoSaving(bool newAutoSave = true);

  void closeList(const string &name);

  void saveList(const string &name);

  void setSavingDir(const filesystem::path &dir, bool clearDir = false);

  void resetSavingDir();

  void setPassword(const string &name, const string &password);

  void setPassword(const string &name, const string &publicPassword, const string &privatePassword);

  void removePassword(const string &name);

  void setRemoteDir(MEGADirectory *dir);

  void resetRemoteDir();

  shared_ptr<List> getList(const string &name);

  List &operator[](const string &name);

  void saveAll();
};

#endif //LISTTEST_LISTSMANAGER_H