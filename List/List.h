//
// Created by nikita on 12/3/18.
//

#ifndef LISTTEST_LIST_H
#define LISTTEST_LIST_H

typedef unsigned char Byte;

#include <unistd.h>
#include <pwd.h>
#include <fstream>
#include <vector>
#include <filesystem>
#include <set>

#include <cryptopp/eccrypto.h>
#include <cryptopp/osrng.h>
#include <cryptopp/gcm.h>
#include <cryptopp/base64.h>
#include <cryptopp/oids.h>

using namespace std;
using namespace CryptoPP;

class List {
  ECIES<ECP>::Encryptor twoEncryptor;
  GCM<AES>::Encryption oneEncryption;

  ECIES<ECP>::Decryptor twoDecryptor;
  GCM<AES>::Decryption oneDecryption;

  string onePasswordKey, writeOnlyKey, readWriteKey;

  bool encrypted = false, twoPasswords = false, writeOnly = false;

  set<string> values;
public:
  string setPassword(const string &newWriteOnlyPassword, const string &newReadWritePassword);

  string setPassword(const string &newPassword);

  void removePassword();

  enum class CheckStatus {
    Wrong, OnePassword, WriteOnlyPassword, ReadWritePassword
  };

  CheckStatus checkKey(const string &password, const string &keyString) const;

  CheckStatus openKey(const string &password, const string &keyString);

  void saveToStream(ostream &file);

  void loadFromStream(istream &file);

  void add(const string &value = string());

  void replace(const string &before, const string &after);

  void remove(const string &name);

  set<string> getValues() const;

};

#endif //LISTTEST_LIST_H