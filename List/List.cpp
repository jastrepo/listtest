#pragma clang diagnostic push
#pragma ide diagnostic ignored "bugprone-unused-raii"
//
// Created by nikita on 12/3/18.
//
#ifdef ENABLE_TEST

#include <catch2/catch.hpp>

#endif

#include "List.h"

AutoSeededRandomPool &getRandomPool() {
  static AutoSeededRandomPool randomPool;
  return randomPool;
}

string toBase64(const string &text) {
  string result;
  StringSource(text, true, new Base64Encoder(new StringSink(result), false));
  return result;
}

string fromBase64(const string &text) {
  string result;
  StringSource(text, true, new Base64Decoder(new StringSink(result)));
  return result;
}

vector<Byte> hashSHA256(const string &text) {
  vector<Byte> result(SHA256::DIGESTSIZE);
  static SHA256 hash;
  StringSource(text, true, new HashFilter(hash, new ArraySink(result.data(), result.size())));
  return result;
}

string encryptAES_ECB(const vector<Byte> &key, const string &text) {
  string result;
  ECB_Mode<AES>::Encryption encryption(key.data(), key.size());
  StringSource(text, true, new StreamTransformationFilter(encryption, new StringSink(result)));
  return toBase64(result);
}

string decryptAES_ECB(const vector<Byte> &key, const string &text) {
  string result;
  ECB_Mode<AES>::Decryption decryption(key.data(), key.size());
  StringSource(fromBase64(text), true, new StreamTransformationFilter(decryption, new StringSink(result)));
  return result;
}

string encryptAES_GCM(GCM<AES>::Encryption oneEncryption, const string &text) {
  string result;
  StringSource(text, true, new AuthenticatedEncryptionFilter(oneEncryption, new StringSink(result)));
  return toBase64(result);
}

string decryptAES_GCM(GCM<AES>::Decryption oneDecryption, const string &text) {
  static const unsigned TAG_SIZE = GCM<AES>::Decryption().TagSize();
  string rawText = fromBase64(text), result;
  if (rawText.size() < TAG_SIZE) {
    throw runtime_error("attempt to decrypt text that doesn't contain MAC");
  }
  AuthenticatedDecryptionFilter filter(oneDecryption, new StringSink(result),
                                       AuthenticatedDecryptionFilter::MAC_AT_BEGIN);
  filter.Put((Byte *) rawText.substr(rawText.size() - TAG_SIZE).data(), TAG_SIZE);
  filter.Put((Byte *) rawText.substr(0, rawText.size() - TAG_SIZE).data(), rawText.size() - TAG_SIZE);
  return result;
}

string encryptECIES(const ECIES<ECP>::Encryptor &twoEncryptor, const string &text) {
  string result;
  StringSource(text, true, new PK_EncryptorFilter(getRandomPool(), twoEncryptor, new StringSink(result)));
  return toBase64(result);
}

string decryptECIES(const ECIES<ECP>::Decryptor &twoDecryptor, const string &text) {
  string result;
  StringSource(fromBase64(text), true, new PK_DecryptorFilter(getRandomPool(), twoDecryptor, new StringSink(result)));
  return result;
}

void generateECIESKey(ECIES<ECP>::Encryptor &twoEncryptor, ECIES<ECP>::Decryptor &twoDecryptor) {
  twoDecryptor.AccessKey().GenerateRandom(getRandomPool(),
                                          MakeParameters(CryptoPP::Name::GroupOID(), CryptoPP::ASN1::secp256r1()));
  twoEncryptor.AccessMaterial().AssignFrom(twoDecryptor.GetMaterial());
}

pair<string, string> generateAESKeyAndIV() {
  string key;
  key.resize(AES::MAX_KEYLENGTH);
  getRandomPool().GenerateBlock((Byte *) key.data(), key.size());

  string IV;
  IV.resize(AES::BLOCKSIZE);
  getRandomPool().GenerateBlock((Byte *) IV.data(), IV.size());
  return {key, IV};
}

void setGCMKeyAndIV(const pair<string, string> &keyAndIV, GCM<AES>::Encryption &oneEncryption,
                    GCM<AES>::Decryption &oneDecryption) {
  oneDecryption.SetKeyWithIV((Byte *) keyAndIV.first.data(), keyAndIV.first.size(), (Byte *) keyAndIV.second.data(),
                             keyAndIV.second.size());
  oneEncryption.SetKeyWithIV((Byte *) keyAndIV.first.data(), keyAndIV.first.size(), (Byte *) keyAndIV.second.data(),
                             keyAndIV.second.size());
}

void setGCMKeyAndIV(const pair<string, string> &keyAndIV, GCM<AES>::Encryption &oneEncryption) {
  oneEncryption.SetKeyWithIV((Byte *) keyAndIV.first.data(), keyAndIV.first.size(), (Byte *) keyAndIV.second.data(),
                             keyAndIV.second.size());
}

string generateRandomString(int maxSize) {
  static string possible =
      "0123456789"
      "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
      "abcdefghijklmnopqrstuvwxyz"
      "~`!@#$%^&*()_+|\\{}[]:\";'<>?,./";
  getRandomPool().Shuffle(possible.begin(), possible.end());
  int size = getRandomPool().GenerateByte() % maxSize;
  string ans(possible.begin(), possible.begin() + size);
  return ans;
}

#ifdef ENABLE_TEST

TEST_CASE("ECB encryption test", "[encryption]") {
  string str;
  vector<Byte> key(SHA256::DIGESTSIZE);

  constexpr int keys = 10, strings = 10, stringSize = 64;
  for (int i = 0; i < keys; i++) {
    key = hashSHA256(generateRandomString(AES::MAX_KEYLENGTH));
    for (int j = 0; j < strings; j++) {
      str = generateRandomString(stringSize);
      REQUIRE(str == decryptAES_ECB(key, encryptAES_ECB(key, str)));
    }
  }
}

TEST_CASE("GCM encryption test", "[encryption]") {
  string str;
  GCM<AES>::Encryption oneEncryption;
  GCM<AES>::Decryption oneDecryption;

  constexpr int keys = 10, strings = 10, stringSize = 64;
  for (int i = 0; i < keys; i++) {
    setGCMKeyAndIV(generateAESKeyAndIV(), oneEncryption, oneDecryption);
    for (int j = 0; j < strings; j++) {
      str = generateRandomString(stringSize);
      REQUIRE(str == decryptAES_GCM(oneDecryption, encryptAES_GCM(oneEncryption, str)));
    }
  }
}

TEST_CASE("ECIES encryption test", "[encryption]") {
  string str;
  ECIES<ECP>::Decryptor twoDecryptor;
  ECIES<ECP>::Encryptor twoEncryptor;

  constexpr int keys = 10, strings = 10, stringSize = 64;
  for (int i = 0; i < keys; i++) {
    generateECIESKey(twoEncryptor, twoDecryptor);
    for (int j = 0; j < strings; j++) {
      str = generateRandomString(stringSize);
      REQUIRE(str == decryptECIES(twoDecryptor, encryptECIES(twoEncryptor, str)));
    }
  }
}

#endif

string List::setPassword(const string &newWriteOnlyPassword, const string &newReadWritePassword) {
  try {
    if (!encrypted || !twoPasswords) {
      generateECIESKey(twoEncryptor, twoDecryptor);

      StringSink sink(writeOnlyKey);
      twoEncryptor.GetKey().Save(sink);

      StringSink sink1(readWriteKey);
      twoDecryptor.GetKey().Save(sink1);

      twoPasswords = encrypted = true;
    }
    string result = encryptAES_ECB(hashSHA256(newWriteOnlyPassword), writeOnlyKey) + '\n' +
                    encryptAES_ECB(hashSHA256(newReadWritePassword), readWriteKey);
    return result;
  } catch (CryptoPP::Exception &e) {
    throw runtime_error("Error in setPassword with two passwords: " + e.GetWhat());
  }
}

string List::setPassword(const string &newPassword) {
  try {
    if (!encrypted || twoPasswords) {
      auto keyAndIV = generateAESKeyAndIV();
      setGCMKeyAndIV(keyAndIV, oneEncryption);
      twoPasswords = false;
      encrypted = true;
      onePasswordKey = toBase64(keyAndIV.first) + ' ' + toBase64(keyAndIV.second);
    }
    string result = encryptAES_ECB(hashSHA256(newPassword), onePasswordKey);
    return result;
  } catch (CryptoPP::Exception &e) {
    throw runtime_error("Error in setPassword with one password: " + e.GetWhat());
  }
}

void List::removePassword() {
  encrypted = false;
}

List::CheckStatus List::checkKey(const string &password, const string &keyString) const {
  try {
    if (keyString.empty()) {
      return CheckStatus::Wrong;
    } else {
      auto passwordHash = hashSHA256(password);
      auto keysSeparator = keyString.find('\n');
      if (keysSeparator == string::npos) {
        string newOnePasswordKey;
        try {
          newOnePasswordKey = decryptAES_ECB(passwordHash, keyString);
        } catch (CryptoPP::Exception &e) {
          return CheckStatus::Wrong;
        }
        if (newOnePasswordKey.find(' ') == string::npos) {
          return CheckStatus::Wrong;
        } else {
          return CheckStatus::OnePassword;
        }
      } else {
        try {
          decryptAES_ECB(passwordHash, keyString.substr(0, keysSeparator));
          return CheckStatus::WriteOnlyPassword;
        } catch (CryptoPP::Exception &e) {
          try {
            decryptAES_ECB(passwordHash, keyString.substr(keysSeparator + 1));
            return CheckStatus::ReadWritePassword;
          } catch (CryptoPP::Exception &e1) {
            return CheckStatus::Wrong;
          }
        }
      }
    }
  } catch (CryptoPP::Exception &e) {
    throw runtime_error("Error in checkKey: " + e.GetWhat());
  }
}

List::CheckStatus List::openKey(const string &password, const string &keyString) {
  try {
    auto status = CheckStatus::Wrong;
    if (!keyString.empty()) {
      auto passwordHash = hashSHA256(password);
      auto keysSeparator = keyString.find('\n');
      if (keysSeparator == string::npos) {
        string newOnePasswordKey;
        try {
          newOnePasswordKey = decryptAES_ECB(passwordHash, keyString);
        } catch (CryptoPP::Exception &e) {
          throw logic_error("wrong password or error in the key file");
        }
        auto keyAndIVSeparator = newOnePasswordKey.find(' ');
        if (keyAndIVSeparator != string::npos) {
          setGCMKeyAndIV({fromBase64(newOnePasswordKey.substr(0, keyAndIVSeparator)),
                          fromBase64(newOnePasswordKey.substr(keyAndIVSeparator + 1))}, oneEncryption, oneDecryption);
          onePasswordKey = newOnePasswordKey;
          status = CheckStatus::OnePassword;
        } else {
          throw logic_error("error in the key file");
        }
      } else {
        try {
          string newWriteOnlyKey = decryptAES_ECB(passwordHash, keyString.substr(0, keysSeparator));

          StringSource source1(newWriteOnlyKey, true);
          twoEncryptor.AccessKey().Load(source1);

          writeOnlyKey.clear();
          readWriteKey.clear();

          StringSink sink(writeOnlyKey);
          twoEncryptor.GetKey().Save(sink);

          writeOnly = true;
          status = CheckStatus::WriteOnlyPassword;
        } catch (CryptoPP::Exception &e) {
          try {
            string newReadWriteKey = decryptAES_ECB(passwordHash, keyString.substr(keysSeparator + 1));

            StringSource source1(newReadWriteKey, true);
            twoDecryptor.AccessKey().Load(source1);
            twoEncryptor.AccessMaterial().AssignFrom(twoDecryptor.GetMaterial());

            writeOnlyKey.clear();
            readWriteKey.clear();

            StringSink sink(writeOnlyKey);
            twoEncryptor.GetKey().Save(sink);

            StringSink sink1(readWriteKey);
            twoDecryptor.GetKey().Save(sink1);

            status = CheckStatus::ReadWritePassword;
          } catch (CryptoPP::Exception &e1) {
            throw logic_error("wrong password or error in the key file");
          }
        }
        twoPasswords = true;
      }
      encrypted = true;
    }
    return status;
  } catch (CryptoPP::Exception &e) {
    throw runtime_error("Error in openKey: " + e.GetWhat());
  }
}

void List::saveToStream(ostream &file) { // TODO: make documentation
  try {
    for (const auto &value:values) {
      if (encrypted && !writeOnly) {
        if (twoPasswords) {
          file << encryptECIES(twoEncryptor, value) << endl;
        } else {
          file << encryptAES_GCM(oneEncryption, value) << endl;
        }
      } else {
        file << value << endl;
      }
    }
  } catch (CryptoPP::Exception &e) {
    throw runtime_error("Error in save: " + e.GetWhat());
  }
}

void List::loadFromStream(istream &file) {
  try {
    values.clear();
    string line;
    getline(file, line);
    while (!(file.fail() || file.eof())) {
      if (writeOnly || !encrypted) {
        values.insert(line);
      } else {
        if (twoPasswords) {
          values.insert(decryptECIES(twoDecryptor, line));
        } else {
          values.insert(decryptAES_GCM(oneDecryption, line));
        }
      }
      getline(file, line);
    }
  } catch (CryptoPP::Exception &e) {
    throw runtime_error("Error in load: " + e.GetWhat());
  }
}

void List::add(const string &value) {
  try {
    if (writeOnly) {
      values.insert(encryptECIES(twoEncryptor, value));
    } else {
      values.insert(value);
    }
  } catch (CryptoPP::Exception &e) {
    throw runtime_error("Error in add: " + e.GetWhat());
  }
}

void List::replace(const string &before, const string &after) {
  if (writeOnly) {
    throw logic_error("the list is write only");
  } else {
    if (before != after) {
      remove(before);
      add(after);
    }
  }
}

void List::remove(const string &name) {
  if (writeOnly) {
    throw logic_error("the list is write only");
  } else {
    auto it = values.find(name);
    if (it == values.end()) {
      throw logic_error(name + " is not in the list");
    }
    values.erase(it);
  }
}

set<string> List::getValues() const {
  if (writeOnly) {
    throw logic_error("the list is write only");
  } else {
    return set<string>(values);
  }
}

#pragma clang diagnostic pop