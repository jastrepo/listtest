//
// Created by nikita on 4/16/19.
//
#ifndef LISTTEST_LISTTEST_H
#define LISTTEST_LISTTEST_H

#include "../List.h"

class ListTestActions;

class ListTest {
  set<string> state;
  shared_ptr<List> list;
  stringstream listStr;
  string keyStr;
public:
  ListTest();

  void recreateList();

  void requireCorrectState() const;

  void requireIncorrectState() const;

  void fill(const set<string> &names);

  void replace(const string &before, const string &after);

  void add(const string &value);

  void remove(const string &value);

  void setPassword(const string &password);

  void setPassword(const string &writeOnlyPassword, const string &readWritePassword);

  void removePassword();

  void requireWrongPassword(const string &password) const;

  void requireWriteOnly() const;

  void save();

  void load();

  void openKey(const string &password);

  ListTestActions operator()();
};

#endif //LISTTEST_LISTTEST_H