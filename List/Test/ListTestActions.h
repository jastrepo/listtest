//
// Created by nikita on 4/20/19.
//
#ifndef LISTTEST_LISTTESTACTIONS_H
#define LISTTEST_LISTTESTACTIONS_H

#include "ListTest.h"

class ListTestActions {
  friend class ListTest;

  class Action {
  public:
    enum class Params {
      None,
      Recreate,
      RequireCorrectState,
      RequireIncorrectState,
      Fill,
      Replace,
      Add,
      Remove,
      SetPassword,
      RemovePassword,
      RequireWrongPassword,
      RequireWriteOnly,
      Save,
      Load,
      OpenKey
    };
    Params params = Params::None;
    string password, writeOnlyPassword;
    string element, newElement;
    bool two = false;
    set<string> elements;
  };

  vector<Action> actions;
  ListTest *listTest;

  explicit ListTestActions(ListTest *newListTest);

public:
  ListTestActions &recreate();

  ListTestActions &requireCorrectState();

  ListTestActions &requireIncorrectState();

  ListTestActions &fill(const set<string> &elements);

  ListTestActions &replace(const string &oldElement, const string &newElement);

  ListTestActions &add(const string &element = string());

  ListTestActions &remove(const string &element);

  ListTestActions &setPassword(const string &password);

  ListTestActions &setPassword(const string &writeOnlyPassword, const string &readWritePassword);

  ListTestActions &removePassword();

  ListTestActions &requireWrongPassword(const string &password);

  ListTestActions &requireWriteOnly();

  ListTestActions &save();

  ListTestActions &load();

  ListTestActions &openKey(const string &password);

  ~ListTestActions();
};

#endif //LISTTEST_LISTTESTACTIONS_H