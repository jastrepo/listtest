//
// Created by nikita on 1/31/19.
//

#include <catch2/catch.hpp>
#include <iostream>
#include "../ListsManager.h"
#include "ListTest.h"
#include "ListTestActions.h"

set<string> getTestStrings() {
  set<string> names;
  string currName;
  for (int i = 0; i < 10; i++) {
    currName = "item" + to_string(i);
    names.insert(currName);
  }
  names.insert("i");
  names.insert("very long something");
  return names;
}

TEST_CASE("Unencrypted list without saving", "[list]") {
  ListTest test;

  test.requireCorrectState();

  test().add().requireCorrectState();

  test().remove("").add("text").requireCorrectState();

  test().remove("text").add("текст").requireCorrectState();

  test().remove("текст").requireCorrectState();

  test().fill(getTestStrings()).requireCorrectState();

  test().add("i").requireCorrectState();

  test().replace("item3", "some text").requireCorrectState();

  test().remove("item5").requireCorrectState();

  test().add("new str").requireCorrectState();
}


TEST_CASE("checking that list doesn't change after saving", "[list]") {
  ListTest test;
  test.fill(getTestStrings());

  test().save().requireCorrectState();
}

TEST_CASE("checking that unsaved changes are lost", "[list]") {
  ListTest test;
  test.fill(getTestStrings());

  test().save().add("item").load().requireIncorrectState();
}

TEST_CASE("checking load several times without saving", "[list]") {
  ListTest test;
  test.fill(getTestStrings());

  test().save();

  test().recreate().load().requireCorrectState();

  test().recreate().load().requireCorrectState();
}

TEST_CASE("checking work after saving loading", "[list]") {
  ListTest test;

  test().fill(getTestStrings()).add("elem");

  test().save().recreate().load().requireCorrectState();

  test.replace("item1", "new item");

  test().save().recreate().load().requireCorrectState();

  test.remove("item4");

  test().save().recreate().load().requireCorrectState();
}

TEST_CASE("saving with russian letters", "[list]") {
  ListTest test;
  test().add("test").add("тест");

  test().save().recreate().load().requireCorrectState();
}

TEST_CASE("checking encryption", "[list]") {
  string password = "somepass";

  ListTest test;
  test.fill(getTestStrings());
  test().setPassword(password).requireCorrectState();

  test().save().recreate().load().requireIncorrectState().recreate();
  test().openKey("somepass").load().requireCorrectState();
}

TEST_CASE("checking wrong password", "[list]") {
  string password = "somepass";

  ListTest test;
  test.fill(getTestStrings());
  test().setPassword(password).requireCorrectState();

  test().save().recreate().load().requireIncorrectState().recreate();
  test.requireWrongPassword("somepass1");
  test.requireWrongPassword("short");
  test.requireWrongPassword("waaaaaaaay too long password for match");
  test().openKey(password).load().requireCorrectState();
}

TEST_CASE("checking short password", "[list]") {
  string password = "s";

  ListTest test;
  test.fill(getTestStrings());
  test().setPassword(password).requireCorrectState();

  test().save().recreate().load().requireIncorrectState().recreate().openKey(password).load().requireCorrectState();
}

TEST_CASE("checking long password", "[list]") {
  string password = "somerandomverylo[][ngstringfo234rpassp[42]hrase";

  ListTest test;
  test.fill(getTestStrings());
  test().setPassword(password).requireCorrectState();

  test().save().recreate().load().requireIncorrectState().recreate().openKey(password).load().requireCorrectState();
}

TEST_CASE("actions with encrypted list", "[list]") {
  string password = "somepass";

  ListTest test;
  test().fill(getTestStrings()).add("elem");
  test.setPassword(password);

  test().save().recreate().load().requireIncorrectState().recreate().openKey(password).load().requireCorrectState();

  test.replace("item1", "new item");

  test().save().recreate().load().requireIncorrectState().recreate().openKey(password).load().requireCorrectState();

  test.remove("item4");

  test().save().recreate().load().requireIncorrectState().recreate().openKey(password).load().requireCorrectState();
}

TEST_CASE("change password", "[list]") {
  string password = "somepass", newPassword = "somenewpass";

  ListTest test;
  test.setPassword(password);
  test.fill(getTestStrings());

  test().save().recreate().load().requireIncorrectState().recreate();
  test().requireWrongPassword(newPassword);
  test().requireWrongPassword("somewrongpass");
  test().openKey(password).load().requireCorrectState();

  test().setPassword(newPassword).requireCorrectState();

  test().save().recreate().load().requireIncorrectState().recreate();
  test().requireWrongPassword(password);
  test().requireWrongPassword("somewrongpass");
  test().openKey(newPassword).load().requireCorrectState();
}

TEST_CASE("remove password", "[list]") {
  string password = "somepass";

  ListTest test;
  test.setPassword(password);
  test.fill(getTestStrings());

  test().save().recreate().load().requireIncorrectState().recreate();
  test().requireWrongPassword("somenewpass");
  test().openKey(password).load().requireCorrectState();

  test().removePassword().requireCorrectState();

  test().save().recreate().load().requireCorrectState();
}

TEST_CASE("checking two password encryption", "[list]") {
  string writeOnlyPassword = "writeonlypass", password = "somepass";

  ListTest test;
  test.fill(getTestStrings());
  test().setPassword(writeOnlyPassword, password).requireCorrectState();

  test().save().recreate().load().requireIncorrectState();
}

TEST_CASE("check decrypt with read write password", "[list]") {
  string writeOnlyPassword = "writeonlypass", password = "somepass";

  ListTest test;
  test.fill(getTestStrings());
  test.setPassword(writeOnlyPassword, password);

  test().save().recreate().openKey(password).load().requireCorrectState();
}

TEST_CASE("check decrypt with write only password", "[list]") {
  string writeOnlyPassword = "writeonlypass", password = "somepass";

  ListTest test;
  test.fill(getTestStrings());
  test.setPassword(writeOnlyPassword, password);

  test().save().recreate().load().requireIncorrectState().recreate();
  test().openKey(writeOnlyPassword).load().requireWriteOnly();
  REQUIRE_THROWS_WITH(test.remove("item6"), Catch::Contains("write"));
  test.add("test");

  test().save().recreate().load().requireIncorrectState().recreate().openKey(password).load().requireCorrectState();
}

TEST_CASE("actions with encrypted by two passwords list in read write mode", "[list]") {
  string writeOnlyPassword = "writeonlypass", password = "somepass";

  ListTest test;
  test().fill(getTestStrings()).add("elem");
  test.setPassword(writeOnlyPassword, password);

  test().save().recreate().load().requireIncorrectState().recreate().openKey(password).load().requireCorrectState();

  test.replace("item1", "new item");

  test().save().recreate().load().requireIncorrectState().recreate().openKey(password).load().requireCorrectState();

  test.remove("item4");

  test().save().recreate().load().requireIncorrectState().recreate().openKey(password).load().requireCorrectState();
}

TEST_CASE("actions with encrypted by two passwords list in both modes", "[list]") {
  string writeOnlyPassword = "writeonlypass", password = "somepass";

  ListTest test;
  test.setPassword(writeOnlyPassword, password);
  test().fill(getTestStrings()).add("elem1");

  test().save().recreate().load().requireIncorrectState().recreate();
  test().openKey(writeOnlyPassword).load().requireWriteOnly();
  test.add("new item");

  test().save().recreate().load().requireIncorrectState().recreate().openKey(password).load().requireCorrectState();
  test.replace("new item", "old item");

  test().save().recreate().load().requireIncorrectState().recreate().openKey(password).load().requireCorrectState();
}

TEST_CASE("change read write password", "[list]") {
  string writeOnlyPassword = "writeonlypass", password = "somepass", newPassword = "somenewpass";

  ListTest test;
  test.setPassword(writeOnlyPassword, password);
  test.fill(getTestStrings());

  test().save().recreate().load().requireIncorrectState().recreate();
  test().requireWrongPassword(newPassword);
  test().openKey(password).load().requireCorrectState();

  test.add("my_item");

  test().setPassword(writeOnlyPassword, newPassword).requireCorrectState();

  test().save().recreate().load().requireIncorrectState().recreate();
  test().requireWrongPassword(password);
  test().openKey(newPassword).load().requireCorrectState();
}

TEST_CASE("change write only password", "[list]") {
  string writeOnlyPassword = "writeonlypass", password = "somepass", newWriteOnlyPassword = "newWriteonlypass";

  ListTest test;
  test.setPassword(writeOnlyPassword, password);
  test.fill(getTestStrings());

  test().save().recreate().load().requireIncorrectState().recreate();
  test().requireWrongPassword(newWriteOnlyPassword);
  test().openKey(writeOnlyPassword).load().requireWriteOnly();

  test.add("text");

  test().save().recreate().load().requireIncorrectState().recreate().openKey(password).load().requireCorrectState();

  test().setPassword(newWriteOnlyPassword, password).requireCorrectState();

  test().save().recreate().load().requireIncorrectState().recreate();
  test().requireWrongPassword(writeOnlyPassword);
  test().openKey(newWriteOnlyPassword).load().requireWriteOnly();

  test.add("test");

  test().save().recreate().load().requireIncorrectState().recreate().openKey(password).load().requireCorrectState();
}

TEST_CASE("remove two passwords", "[list]") {
  string writeOnlyPassword = "writeonlypass", password = "somepass";

  ListTest test;
  test.fill(getTestStrings());
  test.setPassword(writeOnlyPassword, password);

  test().save().recreate().load().requireIncorrectState().recreate();
  test().openKey(writeOnlyPassword).load().requireWriteOnly();

  test.add("elem");

  test().save().recreate().load().requireIncorrectState().recreate();
  test().openKey(password).load().requireCorrectState();

  test.remove("item2");

  test().removePassword().requireCorrectState();

  test.add("new item");

  test().save().recreate().load().requireCorrectState();
}

TEST_CASE("change from two password to one", "[list]") {
  string writeOnlyPassword = "writeonlypass", readWritePassword = "readwritepass", password = "somepass";

  ListTest test;
  test.fill(getTestStrings());
  test.setPassword(writeOnlyPassword, readWritePassword);

  test().save().recreate().load().requireIncorrectState();
  test().recreate().openKey(writeOnlyPassword).load().requireWriteOnly();

  test.add("elem");

  test().save().recreate().load().requireIncorrectState();
  test().recreate().openKey(readWritePassword).load().requireCorrectState();

  test.remove("item2");

  test().setPassword(password).requireCorrectState();

  test().save().recreate().load().requireIncorrectState().recreate();
  test().requireWrongPassword(writeOnlyPassword);
  test().requireWrongPassword(readWritePassword);
  test().openKey(password).load().requireCorrectState();

  test.add("test");

  test().save().recreate().load().requireIncorrectState().openKey(password).load().requireCorrectState();
}

TEST_CASE("change from one password to two", "[list]") {
  string writeOnlyPassword = "writeonlypass", readWritePassword = "readwritepass", password = "somepass";

  ListTest test;
  test.setPassword(password);
  test.fill(getTestStrings());

  test().save().recreate().load().requireIncorrectState().recreate();
  test().requireWrongPassword(writeOnlyPassword);
  test().requireWrongPassword(readWritePassword);
  test().openKey(password).load().requireCorrectState();

  test.replace("item6", "my item");

  test().setPassword(writeOnlyPassword, readWritePassword).requireCorrectState();

  test().save().recreate().load().requireIncorrectState().recreate();
  test().requireWrongPassword(password);
  test().openKey(writeOnlyPassword).load().requireWriteOnly();

  test.add("new item");

  test().save().recreate().load().requireIncorrectState().recreate();
  test().requireWrongPassword(password);
  test().openKey(readWritePassword).load().requireCorrectState();
}

//TODO: file corruption test
//TODO: check randomness of generating keys
//TODO: open list after crash