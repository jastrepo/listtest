//
// Created by nikita on 4/20/19.
//

#include "ListTestActions.h"

ListTestActions::ListTestActions(ListTest *newListTest) : listTest(newListTest) {}

ListTestActions &ListTestActions::recreate() {
  Action a;
  a.params = Action::Params::Recreate;
  actions.push_back(a);
  return *this;
}

ListTestActions &ListTestActions::requireCorrectState() {
  Action a;
  a.params = Action::Params::RequireCorrectState;
  actions.push_back(a);
  return *this;
}

ListTestActions &ListTestActions::requireIncorrectState() {
  Action a;
  a.params = Action::Params::RequireIncorrectState;
  actions.push_back(a);
  return *this;
}

ListTestActions &ListTestActions::fill(const set<string> &elements) {
  Action a;
  a.params = Action::Params::Fill;
  a.elements = elements;
  actions.push_back(a);
  return *this;
}

ListTestActions &ListTestActions::replace(const string &oldElement, const string &newElement) {
  Action a;
  a.params = Action::Params::Replace;
  a.element = oldElement;
  a.newElement = newElement;
  actions.push_back(a);
  return *this;
}

ListTestActions &ListTestActions::add(const string &element) {
  Action a;
  a.params = Action::Params::Add;
  a.element = element;
  actions.push_back(a);
  return *this;
}

ListTestActions &ListTestActions::remove(const string &element) {
  Action a;
  a.params = Action::Params::Remove;
  a.element = element;
  actions.push_back(a);
  return *this;
}

ListTestActions &ListTestActions::setPassword(const string &password) {
  Action a;
  a.params = Action::Params::SetPassword;
  a.two = false;
  a.password = password;
  actions.push_back(a);
  return *this;
}

ListTestActions &ListTestActions::setPassword(const string &writeOnlyPassword, const string &readWritePassword) {
  Action a;
  a.params = Action::Params::SetPassword;
  a.two = true;
  a.password = readWritePassword;
  a.writeOnlyPassword = writeOnlyPassword;
  actions.push_back(a);
  return *this;
}

ListTestActions &ListTestActions::removePassword() {
  Action a;
  a.params = Action::Params::RemovePassword;
  actions.push_back(a);
  return *this;
}

ListTestActions &ListTestActions::requireWrongPassword(const string &password) {
  Action a;
  a.params = Action::Params::RequireWrongPassword;
  a.password = password;
  actions.push_back(a);
  return *this;
}

ListTestActions &ListTestActions::requireWriteOnly() {
  Action a;
  a.params = Action::Params::RequireWriteOnly;
  actions.push_back(a);
  return *this;
}

ListTestActions &ListTestActions::save() {
  Action a;
  a.params = Action::Params::Save;
  actions.push_back(a);
  return *this;
}

ListTestActions &ListTestActions::load() {
  Action a;
  a.params = Action::Params::Load;
  actions.push_back(a);
  return *this;
}

ListTestActions &ListTestActions::openKey(const string &password) {
  Action a;
  a.params = Action::Params::OpenKey;
  a.password = password;
  actions.push_back(a);
  return *this;
}

ListTestActions::~ListTestActions() {
  for (const auto &action:actions) {
    switch (action.params) {
      case Action::Params::None:
        break;
      case Action::Params::Recreate:
        listTest->recreateList();
        break;
      case Action::Params::RequireCorrectState:
        listTest->requireCorrectState();
        break;
      case Action::Params::RequireIncorrectState:
        listTest->requireIncorrectState();
        break;
      case Action::Params::Fill:
        listTest->fill(action.elements);
        break;
      case Action::Params::Replace:
        listTest->replace(action.element, action.newElement);
        break;
      case Action::Params::Add:
        listTest->add(action.element);
        break;
      case Action::Params::Remove:
        listTest->remove(action.element);
        break;
      case Action::Params::SetPassword:
        if (action.two) {
          listTest->setPassword(action.writeOnlyPassword, action.password);
        } else {
          listTest->setPassword(action.password);
        }
        break;
      case Action::Params::RemovePassword:
        listTest->removePassword();
        break;
      case Action::Params::RequireWrongPassword:
        listTest->requireWrongPassword(action.password);
        break;
      case Action::Params::RequireWriteOnly:
        listTest->requireWriteOnly();
        break;
      case Action::Params::Save:
        listTest->save();
        break;
      case Action::Params::Load:
        listTest->load();
        break;
      case Action::Params::OpenKey:
        listTest->openKey(action.password);
        break;
    }
  }
}

