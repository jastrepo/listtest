//
// Created by nikita on 4/16/19.
//
#include "ListTest.h"
#include "ListTestActions.h"
#include <iostream>
#include <catch2/catch.hpp>

ListTest::ListTest() {
  recreateList();
}

void ListTest::recreateList() {
  list = make_shared<List>();
}

void ListTest::requireCorrectState() const {
  REQUIRE(list->getValues() == state);
}

void ListTest::requireIncorrectState() const {
  REQUIRE(list->getValues() != state);
}

void ListTest::fill(const set<string> &names) {
  for (const auto &name:names) {
    list->add(name);
    state.insert(name);
  }
}

void ListTest::replace(const string &before, const string &after) {
  list->replace(before, after);
  state.erase(state.find(before));
  state.insert(after);
}

void ListTest::add(const string &value) {
  list->add(value);
  state.insert(value);
}

void ListTest::remove(const string &value) {
  list->remove(value);
  state.erase(state.find(value));
}

void ListTest::setPassword(const string &password) {
  keyStr = list->setPassword(password);
}

void ListTest::setPassword(const string &writeOnlyPassword, const string &readWritePassword) {
  keyStr = list->setPassword(writeOnlyPassword, readWritePassword);
}

void ListTest::removePassword() {
  list->removePassword();
  keyStr.clear();
}

void ListTest::requireWrongPassword(const string &password) const {
  REQUIRE(list->checkKey(password, keyStr) == List::CheckStatus::Wrong);
}

void ListTest::requireWriteOnly() const {
  REQUIRE_THROWS_WITH(list->getValues(), Catch::Contains("write"));
}

void ListTest::save() {
  listStr.str(string());
  list->saveToStream(listStr);
}

void ListTest::load() {
  list->loadFromStream(listStr);
  listStr.clear();
  listStr.seekg(0);
}

void ListTest::openKey(const string &password) {
  list->openKey(password, keyStr);
}

ListTestActions ListTest::operator()() {
  return ListTestActions(this);
}