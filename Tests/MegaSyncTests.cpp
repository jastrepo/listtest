//
// Created by nikita on 3/8/19.
//

#include <catch2/catch.hpp>
#include "../MEGASync/MEGADirectory.h"

TEST_CASE("test open MEGA localDir", "[.mega][long]") {
  std::remove("/home/nikita/rand/sometetst");
  std::remove("/home/nikita/rand/");
  std::remove("sometetst");

  auto client = new Client;
  client->login("jast.e.new.name@gmail.com", "XD8u23pKRKb?-QDk");

  MEGADirectory dir(client);
  dir.setDirs("/home/nikita/rand/", "listtest1");

  ofstream file("sometetst");
  file.exceptions(ios_base::badbit);
  file << "sometext";
  file.close();

  REQUIRE_THROWS_WITH(dir.putFile("sometetst"), Catch::Contains("exist"));
  std::filesystem::rename("sometetst", "/home/nikita/rand/sometetst");
  dir.putFile("sometetst");

  std::remove("/home/nikita/rand/sometetst");

  dir.getFile("sometetst");
  ifstream file1("sometetst");
  file1.exceptions(ios_base::badbit);
  REQUIRE(file1.fail());
  file1.close();

  file1.open("/home/nikita/rand/sometetst");
  REQUIRE(!file1.fail());
  string s;
  file1 >> s;
  file1.close();
  delete client;

  std::remove("sometetst");
  std::remove("/home/nikita/rand/");
  std::remove("/home/nikita/rand/sometetst");
}
