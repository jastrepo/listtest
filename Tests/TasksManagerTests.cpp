//
// Created by nikita on 4/12/19.
//

#include <catch2/catch.hpp>
#include "../TaskManager/TasksManager.h"

TEST_CASE("operation at task", "[tasksManager]") {
  Task task("task", "some task for test");
  {
    ListsManager manager;
    TasksManager t(manager);
    REQUIRE(t.getIncomplete().empty());
    auto newTask = new Task(task);
    t.addTask(newTask);

    auto comp = t.getCompleted();
    auto tasks = t.getIncomplete();
    auto removed = t.getRemoved();
    REQUIRE(comp.empty());
    REQUIRE(removed.empty());
    REQUIRE(tasks.size() == 1);
    REQUIRE(*(tasks[0]) == task);

    t.completeTask(newTask);

    tasks = t.getIncomplete();
    comp = t.getCompleted();
    removed = t.getRemoved();
    REQUIRE(tasks.empty());
    REQUIRE(removed.empty());
    REQUIRE(comp.size() == 1);
    REQUIRE(*(comp[0]) == task);

    t.removeTask(newTask);

    tasks = t.getIncomplete();
    comp = t.getCompleted();
    removed = t.getRemoved();
    REQUIRE(tasks.empty());
    REQUIRE(comp.empty());
    REQUIRE(removed.size() == 1);
    REQUIRE(*(removed[0]) == task);
  }
}

TEST_CASE("operation at two tasks", "[tasksManager]") {
  Task task("task", "some task for test"), task1("another task", "new task");
  {
    ListsManager manager;
    TasksManager t(manager);
    REQUIRE(t.getIncomplete().empty());
    auto newTask = new Task(task), newTask1 = new Task(task1);
    t.addTask(newTask);

    auto comp = t.getCompleted();
    auto tasks = t.getIncomplete();
    auto removed = t.getRemoved();
    REQUIRE(comp.empty());
    REQUIRE(removed.empty());
    REQUIRE(tasks.size() == 1);
    REQUIRE(*(tasks[0]) == task);

    REQUIRE_THROWS_WITH(t.addTask(newTask), Catch::Contains("already"));
    t.addTask(newTask1);

    tasks = t.getIncomplete();
    comp = t.getCompleted();
    removed = t.getRemoved();
    REQUIRE(comp.empty());
    REQUIRE(removed.empty());
    REQUIRE(tasks.size() == 2);
    REQUIRE(find_if(tasks.begin(), tasks.end(), [task](Task *elem) { return *elem == task; }) != tasks.end());
    REQUIRE(find_if(tasks.begin(), tasks.end(), [task1](Task *elem) { return *elem == task1; }) != tasks.end());

    t.completeTask(newTask);

    tasks = t.getIncomplete();
    comp = t.getCompleted();
    removed = t.getRemoved();
    REQUIRE(removed.empty());
    REQUIRE(tasks.size() == 1);
    REQUIRE(*(tasks[0]) == task1);
    REQUIRE(comp.size() == 1);
    REQUIRE(*(comp[0]) == task);

    t.removeTask(newTask1);

    tasks = t.getIncomplete();
    comp = t.getCompleted();
    removed = t.getRemoved();
    REQUIRE(tasks.empty());
    REQUIRE(comp.size() == 1);
    REQUIRE(*(comp[0]) == task);
    REQUIRE(removed.size() == 1);
    REQUIRE(*(removed[0]) == task1);
  }
}

TEST_CASE("auto saving task", "[tasksManager]") {
  Task task("task", "some task for test");
  {
    ListsManager manager;
    manager.setSavingDir("lists", true);
    manager.forceOpenLists();
    TasksManager t(manager);
    REQUIRE(t.getCompleted().empty());
    REQUIRE(t.getRemoved().empty());
    REQUIRE(t.getIncomplete().empty());
    t.addTask(new Task(task));
  }
  {
    ListsManager manager;
    manager.setSavingDir("lists");
    manager.forceOpenLists();
    TasksManager t(manager);
    REQUIRE(t.getCompleted().empty());
    REQUIRE(t.getRemoved().empty());
    auto tasks = t.getIncomplete();
    REQUIRE_FALSE(tasks.empty());
    REQUIRE(*(tasks[0]) == task);
  }
}

TEST_CASE("auto saving two tasks", "[tasksManager]") {
  Task task("task", "some task for test"), task1("some another task", " just for test ");
  {
    ListsManager manager;
    manager.setSavingDir("lists", true);
    manager.forceOpenLists();
    TasksManager t(manager);
    REQUIRE(t.getCompleted().empty());
    REQUIRE(t.getRemoved().empty());
    REQUIRE(t.getIncomplete().empty());
    t.addTask(new Task(task));
  }
  {
    ListsManager manager;
    manager.setSavingDir("lists");
    manager.forceOpenLists();
    TasksManager t(manager);
    REQUIRE(t.getCompleted().empty());
    REQUIRE(t.getRemoved().empty());
    auto tasks = t.getIncomplete();
    REQUIRE(tasks.size() == 1);
    REQUIRE(*(tasks[0]) == task);
    t.addTask(new Task(task1));
  }
  {
    ListsManager manager;
    manager.setSavingDir("lists");
    manager.forceOpenLists();
    TasksManager t(manager);
    REQUIRE(t.getCompleted().empty());
    REQUIRE(t.getRemoved().empty());
    auto tasks = t.getIncomplete();
    REQUIRE(tasks.size() == 2);
    REQUIRE(find_if(tasks.begin(), tasks.end(), [task](Task *elem) { return *elem == task; }) != tasks.end());
    auto taskPos1 = find_if(tasks.begin(), tasks.end(), [task1](Task *elem) { return *elem == task1; });
    REQUIRE(taskPos1 != tasks.end());
    t.removeTask(*taskPos1);
  }
  {
    ListsManager manager;
    manager.setSavingDir("lists");
    manager.forceOpenLists();
    TasksManager t(manager);
    REQUIRE(t.getCompleted().empty());
    auto removed = t.getRemoved();
    REQUIRE(removed.size() == 1);
    REQUIRE(*(removed[0]) == task1);
    REQUIRE(t.getCompleted().empty());
    auto tasks = t.getIncomplete();
    REQUIRE(tasks.size() == 1);
    REQUIRE(*(tasks[0]) == task);
  }
}