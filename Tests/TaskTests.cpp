//
// Created by nikita on 2/21/19.
//

#include <catch2/catch.hpp>
#include <random>
#include "../TaskManager/Task.h"

unsigned pow(unsigned x, unsigned n) {
  unsigned res = 1;
  for (unsigned i = 0; i < n; i++) {
    res *= x;
  }
  return res;
}

string getStr(unsigned a, unsigned n) {
  string ans;
  ans.resize(n);
  for (unsigned i = 0; i < n; i++) {
    ans[i] = (a % 3 == 0) ? ' ' : (a % 3 == 1) ? 'a' : ';';
    a /= 3;
  }
  return ans;
}

TEST_CASE("test all strings", "[.task][long]") {
  constexpr unsigned letters = 7;
  for (unsigned i = 0; i < letters; i++) {
    for (unsigned i2 = 0; i2 < ::pow(3, i); i2++) {
      for (unsigned i1 = 0; i1 < letters; i1++) {
        for (unsigned i3 = 0; i3 < ::pow(3, i1); i3++) { // TODO: replace with method from std
          string resString, name = getStr(i2, i), comment = getStr(i3, i1);
          {
            Task t(name, comment);
            resString = t.toString();
          }
          {
            auto t = Task(resString);
            REQUIRE(t.getName() == name);
            REQUIRE(t.getComment() == comment);
          }
        }
      }
    }
  }
}

TEST_CASE("empty task", "[task]") {
  string resString;
  {
    Task t;
    resString = t.toString();
  }
  {
    auto t = Task(resString);
    REQUIRE(t.getName().empty());
    REQUIRE(t.getComment().empty());
  }
}

TEST_CASE("common task", "[task]") {
  string resString;
  {
    Task t("name", "comment");
    resString = t.toString();
  }
  {
    auto t = Task(resString);
    REQUIRE(t.getName() == "name");
    REQUIRE(t.getComment() == "comment");
  }
}

TEST_CASE("empty comment", "[task]") {
  string resString;
  {
    Task t("name", "");
    resString = t.toString();
  }
  {
    auto t = Task(resString);
    REQUIRE(t.getName() == "name");
    REQUIRE(t.getComment().empty());
  }
}

TEST_CASE("empty name", "[task]") {
  string resString;
  {
    Task t("", "comment");
    resString = t.toString();
  }
  {
    auto t = Task(resString);
    REQUIRE(t.getName().empty());
    REQUIRE(t.getComment() == "comment");
  }
}

TEST_CASE("name with spaces", "[task]") {
  string resString;
  auto timePoint = chrono::system_clock::now();
  {
    Task t(timePoint, "name with spaces", "comment");
    resString = t.toString();
  }
  {
    auto t = Task(resString);
    REQUIRE(t.getName() == "name with spaces");
    REQUIRE(t.getComment() == "comment");
    REQUIRE(chrono::duration_cast<chrono::seconds>(
        t.getCreationTime().time_since_epoch() - timePoint.time_since_epoch()).count() == 0);
  }
}

TEST_CASE("comment with spaces", "[task]") {
  string resString;

  {
    Task t("name", "comment with spaces");
    resString = t.toString();
  }
  {
    auto t = Task(resString);
    REQUIRE(t.getName() == "name");
    REQUIRE(t.getComment() == "comment with spaces");
  }
}

TEST_CASE("name and comment with spaces", "[task]") {
  string resString;

  {
    Task t("name with spaces", "comment with spaces");
    resString = t.toString();
  }
  {
    auto t = Task(resString);
    REQUIRE(t.getName() == "name with spaces");
    REQUIRE(t.getComment() == "comment with spaces");
  }
}

TEST_CASE("name with spaces and empty comment", "[task]") {
  string resString;

  {
    Task t("name with spaces", "");
    resString = t.toString();
  }
  {
    auto t = Task(resString);
    REQUIRE(t.getName() == "name with spaces");
    REQUIRE(t.getComment().empty());
  }
}

TEST_CASE("empty name and comment with spaces", "[task]") {
  string resString;
  {
    Task t("", "comment with spaces");
    resString = t.toString();
  }
  {
    auto t = Task(resString);
    REQUIRE(t.getName().empty());
    REQUIRE(t.getComment() == "comment with spaces");
  }

}

TEST_CASE("name and comment starts with spaces", "[task]") {
  string resString;
  {
    Task t("name", " comment with spaces");
    resString = t.toString();
  }
  {
    auto t = Task(resString);
    REQUIRE(t.getName() == "name");
    REQUIRE(t.getComment() == " comment with spaces");
  }

}