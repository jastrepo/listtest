//
// Created by nikita on 3/9/19.
//

#include "ReadOnlyComboBox.h"

ReadOnlyComboBox::ReadOnlyComboBox(QWidget *parent) : QComboBox(parent) {}

bool ReadOnlyComboBox::isReadOnly() const {
  return readOnly;
}

void ReadOnlyComboBox::setReadOnly(bool n_readOnly) {
  readOnly = n_readOnly;
}

void ReadOnlyComboBox::mousePressEvent(QMouseEvent *event) {
  if (!readOnly) {
    QComboBox::mousePressEvent(event);
  }
}

void ReadOnlyComboBox::keyPressEvent(QKeyEvent *event) {
  if (!readOnly) {
    QComboBox::keyPressEvent(event);
  }
}

void ReadOnlyComboBox::wheelEvent(QWheelEvent *event) {
  if (!readOnly) {
    QComboBox::wheelEvent(event);
  }
}
