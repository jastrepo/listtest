//
// Created by nikita on 3/8/19.
//

#include "Listtest.h"
#include "ui_listtest.h"

string Listtest::getNameOfListType(Listtest::ListType type) {
  switch (type) {
    case Listtest::ListType::None:
      return "";
    case Listtest::ListType::Incomplete:
      return "Incomplete";
    case Listtest::ListType::Completed:
      return "Completed";
    case Listtest::ListType::Removed:
      return "Removed";
    default:
      throw runtime_error("impossible situation");
  }
}

Listtest::ListType Listtest::getListTypeByName(const string &name) {
  if (name == "Incomplete") {
    return Listtest::ListType::Incomplete;
  } else if (name == "Completed") {
    return Listtest::ListType::Completed;
  } else if (name == "Removed") {
    return Listtest::ListType::Removed;
  } else if (name.empty()) {
    return Listtest::ListType::None;
  }
  throw logic_error("\"" + name + "\" doesn't name a list type");
}

Listtest::Listtest(QWidget *parent) : QMainWindow(parent), ui(new Ui::listtest) {
  ui->setupUi(this);
  manager.forceOpenLists();
  manager.setSavingDir("lists");
  tasksManager = new TasksManager(manager);
  ui->centralwidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
  connect(ui->lists, &QListWidget::currentItemChanged, this, &Listtest::listSelected);
  connect(ui->elements, &QListWidget::currentItemChanged, this, &Listtest::elementSelected);
  connect(ui->type, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &Listtest::typeChanged);
  connect(ui->add, &QPushButton::clicked, this, &Listtest::addPressed);
  connect(ui->edit, &QPushButton::clicked, this, &Listtest::editPressed);
  connect(ui->remove, &QPushButton::clicked, this, &Listtest::removePressed);
  connect(ui->complete, &QPushButton::clicked, this, &Listtest::completePressed);
  ui->lists->setCurrentRow(0);
  typeChanged(0);
  ui->elements->setCurrentRow(0);
}

Listtest::~Listtest() {
  delete ui;
}

vector<Task *> Listtest::getTasks(Listtest::ListType type) {
  vector<Task *> tasks;
  switch (type) {
    case ListType::Incomplete:
      tasks = tasksManager->getIncomplete();
      break;
    case ListType::Completed:
      tasks = tasksManager->getCompleted();
      break;
    case ListType::Removed:
      tasks = tasksManager->getRemoved();
      break;
    case ListType::None:
      break;
  }
  return tasks;
}

void Listtest::updateElements(const vector<Task *> &tasks) {
  displayedElements = tasks;
  ui->remove->setEnabled(false);
  ui->complete->setEnabled(false);
  ui->elements->clear();
  for (const auto &task: tasks) {
    auto item = new QListWidgetItem(QString::fromStdString(task->getName()));
    item->setToolTip(QString::fromStdString(task->getComment()));
    ui->elements->addItem(item);
  }
}

void Listtest::listSelected(QListWidgetItem *item) {
  if (taskViewMode == TaskViewType::Show) {
    listType = getListTypeByName(item->text().toStdString());
    updateElements(getTasks(listType));
  }
}

void Listtest::switchToShowView() {
  ui->lists->setSelectionMode(QAbstractItemView::SingleSelection);
  ui->elements->setSelectionMode(QAbstractItemView::SingleSelection);
  ui->name->setReadOnly(true);
  ui->comment->setReadOnly(true);
  ui->type->setReadOnly(true);
  ui->period->setReadOnly(true);
  ui->activation->setReadOnly(true);
  ui->deadLine->setReadOnly(true);
  ui->start->setReadOnly(true);
  ui->add->setText("Add");
  ui->edit->setText("Edit");
}

void Listtest::switchToEditView() {
  ui->lists->setSelectionMode(QAbstractItemView::NoSelection);
  ui->elements->setSelectionMode(QAbstractItemView::NoSelection);
  ui->name->setReadOnly(false);
  ui->comment->setReadOnly(false);
  ui->type->setReadOnly(false);
  ui->period->setReadOnly(false);
  ui->activation->setReadOnly(false);
  ui->deadLine->setReadOnly(false);
  ui->start->setReadOnly(false);
  ui->add->setText("Save");
  ui->edit->setText("Cancel");
  ui->remove->setEnabled(false);
  ui->complete->setEnabled(false);
}

void Listtest::addPressed() {
  if (taskViewMode == TaskViewType::Show) {
    switchToEditView();
    ui->type->setCurrentIndex(1);
    ui->name->clear();
    ui->comment->clear();
    taskViewMode = TaskViewType::Add;
  } else {
    switchToShowView();
    if (taskViewMode == TaskViewType::Add) {
      taskViewMode = TaskViewType::Show;
      int type = ui->type->currentIndex();
      if (type == 1) {
        editingTask = new Task(ui->name->text().toStdString(), ui->comment->text().toStdString());
      } else if (type == 2) {
        editingTask = new Task(Task::TaskType::Continuous, ui->name->text().toStdString(),
                               ui->comment->text().toStdString());
        const auto &start = ui->start->date(), &end = ui->deadLine->date();
        editingTask->setStart({start.year(), start.month(), start.day()});
        editingTask->setEnd({end.year(), end.month(), end.day()});
      } else if (type == 3) {
        editingTask = new Task(Task::TaskType::DateSpecified, ui->name->text().toStdString(),
                               ui->comment->text().toStdString());
        editingTask->setStartTime(
            chrono::system_clock::from_time_t(ui->activation->dateTime().toSecsSinceEpoch()));
      } else if (type == 4) {
        const auto &start = ui->start->date();
        editingTask = new Task(Task::TaskType::Circled, ui->name->text().toStdString(),
                               ui->comment->text().toStdString());
        editingTask->setStart({start.year(), start.month(), start.day()});
        editingTask->setPeriod(ui->period->text().toInt());
      }
      tasksManager->addTask(editingTask);
      auto item = ui->lists->findItems("Incomplete", Qt::MatchExactly)[0];
      if (ui->lists->currentItem() == item) {
        listSelected(item);
      } else {
        ui->lists->setCurrentItem(item);
      }
      auto it = find_if(displayedElements.begin(), displayedElements.end(),
                        [&](Task *task) { return task == editingTask; });
      if (it == displayedElements.end()) {
        editingTaskIndex = -1;
      } else {
        editingTaskIndex = int(distance(displayedElements.begin(), it));
      }
    } else {
      taskViewMode = TaskViewType::Show;
      editingTask->setName(ui->name->text().toStdString());
      editingTask->setComment(ui->comment->text().toStdString());
      auto type = Task::TaskType(ui->type->currentIndex());
      if (type != editingTask->getType()) {
        editingTask->setType(type);
      }
      if (type == Task::TaskType::Continuous) {
        const auto &start = ui->start->date(), &end = ui->deadLine->date();
        editingTask->setStart({start.year(), start.month(), start.day()});
        editingTask->setEnd({end.year(), end.month(), end.day()});
      } else if (type == Task::TaskType::DateSpecified) {
        editingTask->setStartTime(
            chrono::system_clock::from_time_t(ui->activation->dateTime().toSecsSinceEpoch()));
      } else if (type == Task::TaskType::Circled) {
        const auto &start = ui->start->date();
        editingTask->setStart({start.year(), start.month(), start.day()});
        editingTask->setPeriod(ui->period->value());
      }
      listSelected(ui->lists->currentItem());
    }
    if (editingTaskIndex != -1) {
      auto item = ui->elements->item(editingTaskIndex);
      if (ui->elements->currentItem() == item) {
        elementSelected(item);
      } else {
        ui->elements->setCurrentItem(item);
      }
    }
    editingTask = nullptr;
    editingTaskIndex = -1;
  }
}

void Listtest::editPressed() {
  switch (taskViewMode) {
    case TaskViewType::Show:
      if (ui->elements->currentRow() >= 0) {
        switchToEditView();
        editingTaskIndex = ui->elements->currentRow();
        editingTask = displayedElements[editingTaskIndex];
        taskViewMode = TaskViewType::Edit;
      }
      break;
    case TaskViewType::Add:
    case TaskViewType::Edit:
      switchToShowView();
      editingTask = nullptr;
      editingTaskIndex = -1;
      taskViewMode = TaskViewType::Show;
      elementSelected(ui->elements->currentItem());
      break;
  }
}

void Listtest::removePressed() {
  if (ui->elements->currentRow() >= 0 && listType != ListType::Removed) {
    tasksManager->removeTask(displayedElements[ui->elements->currentRow()]);
    updateElements(getTasks(listType));
  }
}

void Listtest::completePressed() {
  if (ui->elements->currentRow() >= 0 && listType != ListType::Completed) {
    tasksManager->completeTask(displayedElements[ui->elements->currentRow()]);
    updateElements(getTasks(listType));
  }
}

void Listtest::elementSelected(QListWidgetItem *item) {
  if (taskViewMode == TaskViewType::Show) {
    if (item == nullptr) {
      ui->type->setCurrentIndex(0);
      ui->name->clear();
      ui->comment->clear();
      ui->remove->setEnabled(false);
      ui->complete->setEnabled(false);
    } else {
      ui->remove->setEnabled(true);
      ui->complete->setEnabled(true);
      auto selected = displayedElements[ui->elements->currentRow()];
      auto type = selected->getType();
      if (type == Task::TaskType::None) {
        ui->type->setCurrentIndex(0);
      } else {
        ui->type->setCurrentIndex(unsigned(type));
      }
      ui->name->setText(QString::fromStdString(selected->getName()));
      ui->comment->setText(QString::fromStdString(selected->getComment()));
      if (type == Task::TaskType::Continuous) {
        auto start = selected->getStart(), end = selected->getEnd();
        ui->start->setDate({start.year, start.month, start.day});
        ui->deadLine->setDate({end.year, end.month, end.day});
      } else if (type == Task::TaskType::DateSpecified) {
        ui->activation->setDateTime(
            QDateTime::fromSecsSinceEpoch(chrono::system_clock::to_time_t(selected->getStartTime())));
      } else if (type == Task::TaskType::Circled) {
        auto start = selected->getStart();
        ui->start->setDate({start.year, start.month, start.day});
        ui->period->setValue(selected->getPeriod());
      }
    }
  }
}

void Listtest::typeChanged(int index) {
  if (index == 1 || index == 0) {
    ui->activation->hide();
    ui->activationLabel->hide();
    ui->start->hide();
    ui->startLabel->hide();
    ui->deadLine->hide();
    ui->deadlineLabel->hide();
    ui->period->hide();
    ui->periodLabel->hide();
  } else if (index == 2) {
    ui->activation->hide();
    ui->activationLabel->hide();
    ui->start->show();
    ui->startLabel->show();
    ui->deadLine->show();
    ui->deadlineLabel->show();
    ui->period->hide();
    ui->periodLabel->hide();
  } else if (index == 3) {
    ui->activation->show();
    ui->activationLabel->show();
    ui->start->hide();
    ui->startLabel->hide();
    ui->deadLine->hide();
    ui->deadlineLabel->hide();
    ui->period->hide();
    ui->periodLabel->hide();
  } else if (index == 4) {
    ui->activation->hide();
    ui->activationLabel->hide();
    ui->start->show();
    ui->startLabel->show();
    ui->deadLine->hide();
    ui->deadlineLabel->hide();
    ui->period->show();
    ui->periodLabel->show();
  }
  if (taskViewMode != TaskViewType::Show) {
    if (index == 0) {
      ui->type->setCurrentIndex(1);
    }
  }
}
