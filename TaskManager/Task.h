//
// Created by nikita on 2/16/19.
//
#ifndef LISTTEST_TASK_H
#define LISTTEST_TASK_H

#include "../List/List.h"

class Task {
  template<typename T>
  static string toBinaryString(const T &value) {
    return string(reinterpret_cast<const char *>(&value), sizeof(T));
  }

  template<typename T>
  static T fromBinaryString(string &str) {
    T val = *(reinterpret_cast<const T *>(str.data()));
    str = str.substr(sizeof(T));
    return val;
  }

public:
  enum class TaskType : unsigned {
    None, Common, Continuous, DateSpecified, Circled
  };

  struct Date {
    int year = 0, month = 0, day = 0;

    bool operator==(const Date &second) const {
      return year == second.year && month == second.month && day == second.day;
    }

    void setString(string &str) {
      year = fromBinaryString<int>(str);
      month = fromBinaryString<int>(str);
      day = fromBinaryString<int>(str);
    }

    string getString() const {
      return toBinaryString(year) + toBinaryString(month) + toBinaryString(day);
    }
  };

private:
  shared_ptr<List> myList;
  string lastStr;
  using timePoint=chrono::time_point<chrono::system_clock>;
  timePoint creationTime, startTime;
  set<string> tags;
  string name, comment;
  Date start, end;
  int period = 0;
  TaskType type = TaskType::None;

  void updateLastStr();

  string stringType() const;

public:
  Task();

  virtual ~Task() = default;

  Task(const timePoint &creationTime, TaskType type, string name, string comment);

  Task(const timePoint &creationTime, string name, string comment);

  Task(TaskType type, string name, string comment);

  Task(string name, string comment);

  Task(const Task &second);

  virtual void fromString(string &str);

  virtual string toString() const;

  explicit Task(const string &str);

  explicit operator string() const;

  void setList(shared_ptr<List> list);

  void removeFromList();

  virtual bool operator==(const Task &second) const;

  bool operator>(const Task &second) const; // TODO: make test

  TaskType getType() const;

  void setType(TaskType newType);

  timePoint getCreationTime() const;

  string getName() const;

  void setName(const string &name);

  string getComment() const;

  void setComment(const string &comment);

  Date getStart() const;

  void setStart(Date newStart);

  Date getEnd() const;

  void setEnd(Date newEnd);

  timePoint getStartTime() const;

  void setStartTime(const timePoint &newStartTime);

  int getPeriod() const;

  void setPeriod(int newPeriod);

  vector<string> getTags() const;

  void addTag(const string &tag);

  void removeTag(const string &tag);
};

#endif //LISTTEST_TASK_H