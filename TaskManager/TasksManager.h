//
// Created by nikita on 2/15/19.
//

#ifndef LISTTEST_TASKSMANAGER_H
#define LISTTEST_TASKSMANAGER_H

#include "Task.h"
#include "../List/ListsManager.h"

using namespace std;

class TasksManager {
  enum ListType : unsigned {
    Removed = 0, Completed, Incomplete, Size
  };
  array<shared_ptr<List>, ListType::Size> lists;
  set<pair<Task *, ListType>, function<bool(pair<Task *, ListType>, pair<Task *, ListType>)>> tasks;
  vector<Task *> typedTasks[ListType::Size];

  void addTaskToList(Task *task, ListType type);

  void removeTaskFromList(Task *task);

public:
  explicit TasksManager(ListsManager &manager);

  ~TasksManager();

  vector<Task *> getIncomplete();

  vector<Task *> getCompleted();

  vector<Task *> getRemoved();

  void addTask(Task *newTask);

  void completeTask(Task *task);

  void removeTask(Task *task);

};

#endif //LISTTEST_TASKSMANAGER_H