//
// Created by nikita on 2/15/19.
//

#include "TasksManager.h"

void TasksManager::addTaskToList(Task *task, TasksManager::ListType type) {
  if (tasks.insert({task, type}).second) {
    typedTasks[type].push_back(task);
    task->setList(lists[type]);
  } else {
    throw logic_error("task already added");
  }
}

void TasksManager::removeTaskFromList(Task *task) {
  auto itInTasks = tasks.find({task, Incomplete});
  if (itInTasks != tasks.end()) {
    auto &list = typedTasks[(*itInTasks).second];
    tasks.erase(itInTasks);
    auto itInList = find(list.begin(), list.end(), task);
    if (itInList != list.end()) {
      list.erase(itInList);
    }
    task->removeFromList();
  }
}

TasksManager::TasksManager(ListsManager &manager) {

  tasks = set<pair<Task *, ListType>, function<bool(pair<Task *, ListType>, pair<Task *, ListType>)>>(
      [](pair<Task *, ListType> first, pair<Task *, ListType> second) {
        return first.first->operator>(*(second.first));
      });
  lists[0] = manager.addList("removed");
  lists[1] = manager.addList("completed");
  lists[2] = manager.addList("incomplete");
  for (size_t i = 0; i < lists.size(); i++) {
    auto list = lists[i]->getValues();
    for (const auto &str:list) {
      Task *task = new Task(str);
      tasks.insert({task, ListType(i)});
      typedTasks[i].push_back(task);
      task->setList(lists[i]);
    }

  }
}

TasksManager::~TasksManager() {
  for (auto i:tasks) {
    delete i.first;
  }
}

vector<Task *> TasksManager::getIncomplete() {
  return vector<Task *>(typedTasks[Incomplete].begin(), typedTasks[Incomplete].end());
}

vector<Task *> TasksManager::getCompleted() {
  return vector<Task *>(typedTasks[Completed].begin(), typedTasks[Completed].end());
}

vector<Task *> TasksManager::getRemoved() {
  return vector<Task *>(typedTasks[Removed].begin(), typedTasks[Removed].end());
}

void TasksManager::addTask(Task *newTask) {
  addTaskToList(newTask, Incomplete);
}

void TasksManager::completeTask(Task *task) {
  removeTaskFromList(task);
  addTaskToList(task, Completed);
}

void TasksManager::removeTask(Task *task) {
  removeTaskFromList(task);
  addTaskToList(task, Removed);
}
