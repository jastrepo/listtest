//
// Created by nikita on 2/16/19.
//

#include <iostream>
#include <catch2/catch.hpp>
#include "Task.h"

string getFromString(string &str) {
  string name;
  name.resize(str.size() - 2);
  bool lastSpace = false, nameFilled = false;
  unsigned i, j = 0;
  for (i = 0; i < str.size() && !nameFilled; i++) {
    if (str[i] == ' ') {
      if (lastSpace) {
        name[j++] = ' ';
        lastSpace = false;
      } else {
        lastSpace = true;
      }
    } else {
      if (lastSpace) {
        if (str[i] == ';') {
          nameFilled = true;
        } else {
          throw logic_error("some err");
        }
      } else {
        name[j++] = str[i];
      }
    }
  }
  name.resize(j);
  str = str.substr(i);
  return name;
}

string putToString(const string &str) {
  string res;
  res.resize(str.size() * 2 + 2);
  unsigned i = 0;
  for (const auto &c:str) {
    if (c == ' ') {
      res[i++] = ' ';
    }
    res[i++] = c;
  }
  res[i++] = ' ';
  res[i++] = ';';
  res.resize(i);
  return res;
}

void Task::updateLastStr() {
  string res = toBinaryString(chrono::system_clock::to_time_t(creationTime));
  res += char(type);
  switch (type) {
    case TaskType::None:
      break;
    case TaskType::Common:
      break;
    case TaskType::Continuous:
      res += start.getString();
      res += end.getString();
      break;
    case TaskType::Circled:
      res += start.getString();
      res += toBinaryString(period);
      break;
    case TaskType::DateSpecified:
      res += toBinaryString(chrono::system_clock::to_time_t(startTime));
      break;
  }
  res += putToString(name) + putToString(comment);
  for (const auto &tag:tags) {
    res += putToString(tag);
  }
  if (lastStr != res) {
    if (myList != nullptr) {
      myList->replace(lastStr, res);
    }
    lastStr = res;
  }
}

string Task::stringType() const {
  switch (type) {
    case TaskType::None:
      return "None";
    case TaskType::Common:
      return "Common";
    case TaskType::Continuous:
      return "Continuous";
    case TaskType::Circled:
      return "Circled";
    case TaskType::DateSpecified:
      return "DateSpecified";
  }
  return "Impossible";
}

Task::Task() {
  updateLastStr();
}

Task::Task(const timePoint &creationTime, TaskType type, string name, string comment) :
    creationTime(creationTime), name(move(name)), comment(move(comment)), type(type) {
  updateLastStr();
}

Task::Task(const timePoint &creationTime, string name, string comment) :
    creationTime(creationTime), name(move(name)), comment(move(comment)), type(TaskType::Common) {
  updateLastStr();
}

Task::Task(TaskType type, string name, string comment) :
    creationTime(chrono::system_clock::now()), name(move(name)), comment(move(comment)), type(type) {
  updateLastStr();
}

Task::Task(string name, string comment) :
    creationTime(chrono::system_clock::now()), name(move(name)), comment(move(comment)), type(TaskType::Common) {
  updateLastStr();
}

Task::Task(const Task &second) :
    creationTime(second.creationTime), name(second.name), comment(second.comment), type(second.type) {
  updateLastStr();
}

void Task::fromString(string &str) {
  lastStr = str;
  creationTime = chrono::system_clock::from_time_t(fromBinaryString<time_t>(str));
  type = TaskType(str[0]);
  str = str.substr(1);
  switch (type) {
    case TaskType::None:
      break;
    case TaskType::Common:
      break;
    case TaskType::Continuous:
      start.setString(str);
      end.setString(str);
      break;
    case TaskType::Circled:
      start.setString(str);
      period = fromBinaryString<int>(str);
      break;
    case TaskType::DateSpecified:
      startTime = chrono::system_clock::from_time_t(fromBinaryString<time_t>(str));
      break;
  }
  name = getFromString(str);
  comment = getFromString(str);
  tags.clear();
  while (!str.empty()) {
    tags.insert(getFromString(str));
  }
}

string Task::toString() const {
  return lastStr;
}

Task::Task(const string &str) {
  string str1 = str;
  fromString(str1);
}

Task::operator string() const {
  return toString();
}

void Task::setList(shared_ptr<List> list) {
  if (list != nullptr) {
    myList = list;
    myList->add(toString());
  }
}

void Task::removeFromList() {
  if (myList != nullptr) {
    myList->remove(toString());
    myList = nullptr;
  }
}

bool Task::operator==(const Task &second) const {
  return type == second.type && name == second.name && comment == second.comment;
}

bool Task::operator>(const Task &second) const {
  return lastStr > second.lastStr;
}

Task::TaskType Task::getType() const {
  return type;
}

void Task::setType(Task::TaskType newType) {
  type = newType;
  updateLastStr();
}

Task::timePoint Task::getCreationTime() const {
  return creationTime;
}

string Task::getName() const {
  return name;
}

void Task::setName(const string &n_name) {
  name = n_name;
  updateLastStr();
}

string Task::getComment() const {
  return comment;
}

void Task::setComment(const string &n_comment) {
  comment = n_comment;
  updateLastStr();
}

Task::Date Task::getStart() const {
  if (type == TaskType::Continuous || type == TaskType::Circled) {
    return start;
  } else {
    throw logic_error("start in " + stringType() + " is senseless");
  }
}

void Task::setStart(Task::Date newStart) {
  if (type == TaskType::Continuous || type == TaskType::Circled) {
    start = newStart;
    updateLastStr();
  } else {
    throw logic_error("start in " + stringType() + " is senseless");
  }
}

Task::Date Task::getEnd() const {
  if (type == TaskType::Continuous) {
    return end;
  } else {
    throw logic_error("end in " + stringType() + " is senseless");
  }
}

void Task::setEnd(Task::Date newEnd) {
  if (type == TaskType::Continuous) {
    end = newEnd;
    updateLastStr();
  } else {
    throw logic_error("end in " + stringType() + " is senseless");
  }
}

Task::timePoint Task::getStartTime() const {
  if (type == TaskType::DateSpecified) {
    return startTime;
  } else {
    throw logic_error("start time in " + stringType() + " is senseless");
  }
}

void Task::setStartTime(const Task::timePoint &newStartTime) {
  if (type == TaskType::DateSpecified) {
    startTime = newStartTime;
    updateLastStr();
  } else {
    throw logic_error("start time in " + stringType() + " is senseless");
  }
}

int Task::getPeriod() const {
  if (type == TaskType::Circled) {
    return period;
  } else {
    throw logic_error("period in " + stringType() + " is senseless");
  }
}

void Task::setPeriod(int newPeriod) {
  if (type == TaskType::Circled) {
    period = newPeriod;
    updateLastStr();
  } else {
    throw logic_error("period in " + stringType() + " is senseless");
  }
}

vector<string> Task::getTags() const {
  return vector<string>(tags.begin(), tags.end());
}

void Task::addTag(const string &tag) {
  tags.insert(tag);
}

void Task::removeTag(const string &tag) {
  auto it = tags.find(tag);
  if (it == tags.end()) {
    throw logic_error(tag + " is not in tags list");
  }
  tags.erase(it);
}
