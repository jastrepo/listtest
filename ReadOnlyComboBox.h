//
// Created by nikita on 3/9/19.
//

#ifndef LISTTEST_READONLYCOMBOBOX_H
#define LISTTEST_READONLYCOMBOBOX_H

#include <QComboBox>

class ReadOnlyComboBox : public QComboBox {
Q_OBJECT
Q_PROPERTY(bool readOnly READ isReadOnly WRITE setReadOnly)
  bool readOnly = false;
public:
  explicit ReadOnlyComboBox(QWidget *parent = nullptr);

  bool isReadOnly() const;

  void setReadOnly(bool readOnly);

protected:
  void mousePressEvent(QMouseEvent *event) override;

  void keyPressEvent(QKeyEvent *event) override;

  void wheelEvent(QWheelEvent *event) override;
};


#endif //LISTTEST_READONLYCOMBOBOX_H
