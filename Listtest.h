//
// Created by nikita on 3/8/19.
//

#ifndef LISTTEST_LISTTEST_H
#define LISTTEST_LISTTEST_H

#include <QtWidgets>
#include "TaskManager/TasksManager.h"

namespace Ui {
  class listtest;
}
class Listtest : public QMainWindow {
Q_OBJECT
  enum class ListType : unsigned {
    None, Incomplete, Completed, Removed
  };
  ListType listType = ListType::None;
  enum class TaskViewType : unsigned {
    Show, Add, Edit
  };
  TaskViewType taskViewMode = TaskViewType::Show;
  TasksManager *tasksManager;
  ListsManager manager;
  vector<Task *> displayedElements;
  Task *editingTask = nullptr;
  int editingTaskIndex = -1;
  Ui::listtest *ui;

  static string getNameOfListType(ListType type);

  static ListType getListTypeByName(const string &name);

  vector<Task *> getTasks(ListType type);

  void updateElements(const vector<Task *> &tasks);

  void switchToShowView();

  void switchToEditView();

public:
  explicit Listtest(QWidget *parent = 0);

  ~Listtest() override;

public slots:

  void listSelected(QListWidgetItem *item);

  void elementSelected(QListWidgetItem *item);

  void addPressed();

  void editPressed();

  void removePressed();

  void completePressed();

  void typeChanged(int index);
};
#endif //LISTTEST_LISTTEST_H